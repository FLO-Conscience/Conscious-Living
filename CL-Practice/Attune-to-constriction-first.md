Because it is so easy and common to deny constriction (especially when subtle), we start core CL practice by assuming we do have some constriction. Then, we accept the constriction and take steps to release it so that we can see the situation from a more neutral and relaxed perspective.

The go-to default CL routine is a specific instance of attune-resolve-review focused on constriction:

1. Attune to constriction: What constrictions do I feel?
2. Resolve constriction-about-constriction: Can I accept feeling just what I feel?
3. Release remaining constriction: Use appropriate resolution tools to shift to a less-constricted state

Having noted the messages from the constriction, we can then evaluate things from a more relaxed, open perspective and decide what to do next.

*Postulate: When we engage and make decisions without first accepting and releasing constrictions, we will get continued conflict. When we accept and release constrictions first, we can more reliably make decisions that lead to resolutions with less lingering or new issues.*
