# FLO Conscious Living

This project collects mental models and practical tools to support healthy ways of living based in conscious presence and compassion. We aspire to have CL concepts comprehensive enough and expressed well enough to work for anyone in any situation.

What makes this project different from countless other sources of related ideas? We do not aim to just add yet another book or website or podcast to the overwhelming quantity already available. Instead, FLO-CL aims to be a holistic, evolving, collaborative resource. Instead of merely making a giant collection of ideas; we curate, adapt, and synthesize concepts to be as useful and effective as possible.

Using Free/Libre/Open (FLO) principles and processes means no artificial barriers to accessing, using, modifying, and sharing. As you read, consider the adage of leaving a place better than you find it. Whenever and wherever you have questions or confusion or see room for improvements, translations, or other adaptations, we welcome you to [collaborate with us](FLO-CL-development/CONTRIBUTING.md).

## Contents

(unlinked contents not yet drafted or finalized)

- [Preface: The Limits of Paradigms](CL-intro-and-caveats.md)
- [Conscious Living (CL)](Conscious-Living.md)
    - [Principles](CL-Principles/Conscious-Living-Principles.md)
        - [Attune-Resolve-Review](CL-Principles/Attune-Resolve-Review/Attune-Resolve-Review.md)
            - [Attuning](CL-Principles/Attune-Resolve-Review/Attuning.md)
            - [Resolving](CL-Principles/Attune-Resolve-Review/Resolving.md)
            - [Reviewing](CL-Principles/Attune-Resolve-Review/Reviewing.md)
            - [Healthy & Appropriate](CL-Principles/Attune-Resolve-Review/Healthy-Appropriate.md)
        - [Context vs Content](CL-Principles/Context-vs-Content/Context-vs-Content.md)
            - [Constriction & Expansion](CL-Principles/Context-vs-Content/Constriction-and-Expansion/Constriction-and-Expansion.md)
            - [Stories](CL-Principles/Context-vs-Content/Stories/Stories.md)
                - [Conflict vs Growth Triangles](CL-Principles/Context-vs-Content/Stories/Triangle-archetypes-conflict-vs-growth.md)
            - [Style](CL-Principles/Context-vs-Content/Style/Style.md)
                - [General Attitudes](CL-Principles/Context-vs-Content/Style/Attitudes.md)
                - [Thinking Styles](CL-Principles/Context-vs-Content/Style/Thinking-styles.md)
                    - [Modeling Styles](Model-styles.md)
                - [Language Styles](CL-Principles/Context-vs-Content/Style/Language.md)
                - Voice, tone, and music
                - Kinesthetic styles
                - Visual styles
                - [Media Styles](CL-Principles/Context-vs-Content/Style/Media-styles.md)
                - Task, tool, and design styles
                - Social-organizing styles
            - [Metaphors](CL-Principles/Context-vs-Content/Metaphors/Metaphors.md)
                - [The Stream](CL-Principles/Context-vs-Content/Metaphors/The-Stream.md)
                - [Reflection](CL-Principles/Context-vs-Content/Metaphors/Reflection.md)
            - Traps, habits, and ghosts
                - Trapping
                - Habits
                - Ghosts
        - Commitments
            - Presence
                - [Consciousness and Acceptance](CL-Principles/CL-Commitments/Presence/Consciousness-and-Acceptance.md)
                - [Observation vs Imagination](CL-Principles/CL-Commitments/Presence/Observation-vs-Imagination.md)
                - [Mindfulness](CL-Principles/CL-Commitments/Presence/Mindfulness.md)
                - [Non-dual Awareness](CL-Principles/CL-Commitments/Presence/Non-dual-awareness.md)
            - Responsibility
            - Curiosity
            - Integrity
            - Love & Compassion
            - Vitality
    - Practice
    - History
        - [CLG to CL](CL-history/CLG-to-CL.md)
        - [CHANGELOG](CL-history/CHANGELOG.md)
    - FLO-CL project development
        - [Contributing guide](FLO-CL-development/CONTRIBUTING.md)
        - [FLO-CL project style-guide](FLO-CL-development/STYLE-GUIDE.md)

## Authors and sources

Except where marked otherwise, the files here (so far) were developed by Aaron Wolf and Praveen Venkataramana. The concepts draw from *many* sources, most notably the Conscious Leadership Group. See [AUTHORS-SOURCES.md](AUTHORS-SOURCES.md) for further details.

Note on point-of-view and pronouns: The writing tends to use first-person plural "we". In some cases, "we" means the authors. More often, "we" means both authors and readers or more broadly "we people" (as in everyone in the world).

## License

Files in the main branch here are under the terms of [CC BY-SA](CC-BY-SA.md), the Creative Commons Attribution Share-Alike International License v4.0. This is the most common Free/Libre/Open (FLO) license for non-software (non-code) projects and is the license used by Wikipedia.

The following is an informal summary:

The "BY" refers to **attribution**, retaining the author's name (as identified on the work or noted in instructions where published) and a reference URL for accessing the original work. For this project, attribution should credit "FLO Conscious Living" and always link to `https://codeberg.org/FLO-Conscience/Conscious-Living` (though that link may be updated in the future, and then credits may also get updated though updating is not required).

The "SA" refers to **share-alike**, meaning that any redistribution or adaptations (clearly marked as modified in that case) of the work must also use and indicate the same CC BY-SA license, passing on the freedoms to others.

Besides those requirements, anyone is **free to use, adapt, and share** any and all of the work in any context.

## Community and conduct

While anyone can discuss CL in whatever contexts or groups, we have a dedicated community space in the Matrix.org system (a FLO chat protocol) at https://matrix.to/#/#conscious-living:matrix.org (within an existing Matrix client, you only need the `#conscious-living:matrix.org` part). There, we have public rooms: one to discuss CL concepts and another to discuss using CL in practice (actual life situations and so on). Participants can also set up private rooms for one-on-one or small group discussion.

As with all online communication, we urge everyone to use appropriate caution in discussing anything personal and sensitive.

The [Code of Conscious Conduct](CONDUCT.md) applies to all communication and interactions around the FLO-CL project.

## Note on safety and getting professional support

The CL concepts explore the full range of life experiences. Some of us may face challenges that are unsafe to work on without help. At each moment in our lives, we can consider whether to study and practice in private or with a group or with guidance from a coach/counselor/therapist/etc. May the CL concepts themselves prompt us in making the best choices.

The CL concepts are not certified by any authorities, and there are no guarantees about experiences or results. Everyone remains responsible for approaching these ideas with appropriate care. When in doubt, please get appropriate assistance.

When seeking support, we encourage careful consideration and due diligence, evaluating risks and costs. Although professional certification cannot guarantee someone as an appropriate guide, it does assure that they have completed a regimin of relevant study and training, and it can include legal fiduciary requirements (the duty to act in the interests of clients, unless doing so threatens anyone's basic safety).
