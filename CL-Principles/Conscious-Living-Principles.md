# Principles of Conscious Living (CL)

## Attune-Resolve-Review

CL starts with *attuning* to our thoughts and feelings, responding as skillfully as we can to each circumstance, and continually reviewing and learning.

### Healthy and Appropriate

We can assess our reactions and decisions in terms of how healthy and appropriate they are for the circumstances. We can assess how well we resolve issues without making things worse. We can work toward an overall direction of thriving and well-being.

## Context vs Content

CL applies to whatever topics and details happen in any aspect of our lives. CL does not directly include anything about most *content* of our lives. Instead, CL emphasizes the *context* — the *ways* we work and *how* we experience and process whatever topics. Whatever thoughts, decisions, or ideas we engage with, we come to them from our particular perspective(s) at the time.

### Constriction and Expansion

The primary context we consider in CL is the ever-shifting energies of constriction and expansion. Constriction is energy of closing, holding, and protecting. Expansion is energy of opening, revealing, and sharing. They can both be at higher and lower intensity. We experience constriction and expansion directly as raw sensations, and we can also use them as a metaphorical framing for understanding our thought patterns.

### Other contextual aspects

Other contextual factors considered in CL include styles, stories and beliefs, metaphors, and more.

## Commitments

The CL commitments describe optimal ways to thrive, to live well and ethically in harmony with ourselves and the broader world.

We cannot perfectly align with the commitments all the time. We can see them as ideals to aspire towards, and we can assess our status for each of them in each moment. 

### Presence

Mindful attention can bring insight and freedom. We can bring awareness to thoughts and feelings and accept them just as they are.

### Responsibility

In any situation, we have the ability to respond. We can focus on understanding our options and on making choices using the best methods we have.

### Curiosity

Every situation offers potential for learning. We can ask questions, investigate, and be open to changing our minds.

### Integrity

Our lives and experiences have many interacting parts. Integrity means having our actions, expressions, intentions, agreements, and values all in alignment.

### Love and Compassion

We can hold sincere care and well-wishes toward anyone and anything. When we engage with love and compassion, we free ourselves from toxic patterns of contempt and ill will. We can take care to have helpful rather than harmful impacts on the world.

### Vitality

We thrive when we follow the natural rhythms and patterns of healthy living.
