# Context vs Content 

Wherever attention focuses in any moment, we experience it within a broader context. *Focus* is itself a *visual* reference. Our eyes fixate and focus within a bigger visual field. The same happens with all forms of thoughts and sensations. Attention emphasizes some parts of conscious experience while other parts remain more peripheral.

A related phrase is *figure and ground*. Within a scene, we treat some elements as *foreground*, focusing on them as the main features. Other aspects we treat as *background*. Foreground/background itself uses visual-spatial metaphor of front and back. What we metaphorically *face*, where we point attention, that becomes the *figure* in our experience.

We can think of attention a somewhat like a flashlight. The range can be narrow or wide. The light can stay in place or move around. At the edges, the light may fade gently or may cut off abruptly.

## Unconscious vs conscious contexts

Presumably, what we experience consciously occurs within wider unconscious contexts. For one thing, as attention shifts around, we imagine that most features that enter conscious awareness *persist* even when attention moves away from them. Beyond that, some aspects of context are beyond our capacity to *ever* consciously sense though we may be able to *conceive of* them. Further still, some aspects of context may be forever unconscious and inconceivable. 

CL only deals with what we can experience consciously.

## Narrow or wide focus

At the smallest conscious levels, the very next thought or sensation fits into the contexts of just-prior events and of other concurrent thoughts and sensations. From a broader view, we can the overall contexts of entire scenes or situations. A task is part of a bigger project, and the project is part of a general area of life. Our whole life story fits within the contexts of history and biology and so on.

Height can work as a visual-spatial metaphor here. We might use descriptions like 100-foot vs 1,000-foot vs 10,000-foot views. Whether in an airplane or on a mountaintop, a view from up high gives us much broader perspective. Seeing Earth from space brings an even wider view. At the other end, low-level close-up perspectives bring out much more details while having a much narrower range.

## Attending to context

CL emphasizes the importance of contexts. Besides asking *what* we experience as the main content of a situation, we can ask *how* we experience it. What else is happening alongside and around our focus? Within what attitude, mindset, mood, settings, feelings, perspectives, and beliefs do we experience things?

We can explore context by bringing a topic to mind and then observing what shows up along with it. The process is like fixating our vision on something and then shifting attention to the surroundings in our peripheral vision.

## Shifting content and context

Because we can focus attention on anything within consciousness, we cannot define generically which features are content and which are context. We might focus on a relationship and then notice feelings of stress as part of the relationship's context, or we might focus on stress and see the relationship as context for the stress.

Once we notice how we experience content versus context, we can explore and play. We can widen or narrow our focus and can move it around. We can also do a complete flip of content and context, as in the perceptual shift of a *figure-ground reversal*.

## Common patterns of content and context

Though we can focus on anything, most people have predictable patterns for what gets emphasis and what gets left in the background.

For most of us, CL ideas are *contextual*. Most of the time, our primary activity is not thinking about and working on CL.

Of course, we can treat CL topics as a main focus. Those who work as coaches, therapists, or religious leaders may spend much of their time focusing on CL-related ideas. For them, attuning to *context* might mean acknowledging the details of specific activities, taking time to see things from more typical perspectives.

## Notable usually-contextual features

CL articles include a series highlighting features and patterns that commonly contextual for most of us most of the time.

Among many typically-contextual features, the main one noted in CL is [constriction and expansion](Constriction-and-Expansion/Constriction-and-Expansion.md). Other noted contexts include [style](Style/Style.md), [stories](Stories/Stories.md), [metaphors](Metaphors/Metaphors.md), and more.

When we understand these features, we can notice patterns. For example, constriction may show up more with some topics than with others.

We can also play with changing the contexts. We can try different styles and adjust stories and see what else changes with them.

## Mindfulness as a context

Consciousness itself is context for everything we experience. Bringing awareness to being conscious is called [*mindfulness*](/CL-Principles/CL-Commitments/Presence/Mindfulness.md) (or *meta-awareness*, awareness of being aware). Whatever the main focus within consciousness, it can show up in a mindful context or not.

Mindfulness and related ideas are discussed further in CL within the *presence* commitment.

## Duality of content-context

Content and context, figure and ground, are like opposite sides of the same coin. We understand them in relation to each other. We understand day in contrast to night. Yin and yang are different yet inherently intertwined. There is no up without down, no back without front. Still, front and back *feel* different…

We *can* experience awareness in a way that goes beyond the content-context distinction. Our focus can widen so much that it includes everything in consciousness. If we then release the content-context distinction, we can experience no highlighting, no foreground and background, just everything with equal status. Such a view open-to-everything is an altered state of consciousness we might experience in the course of some types of meditation practices.
