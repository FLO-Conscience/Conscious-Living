# Media styles

Our choice of media and the styles within media affect how we share and experience ideas. Different media have different features across production, publication, and use. Different media are more or less *accessible* to different people depending on factors like sensory impairments or different access to technology.

## In-person communication

When people share ideas in-person, the experience is multi-sensory and responsive. Being present together at a particular time and place helps reduce outside distraction. Communication can be personalized and styles adapted to the particular people and their particular state right at that moment.

Styles in in-person communication include language, tone, kinesthetic, visual, and so many other aspects. How formal or casual is a meeting, how planned or improvised, everyone sitting or standing or walking… One question is whether people also use other media such as written materials. Another important factor is how many people are present. The greater the crowd size, the less chance for everyone to participate.

## Live video communication

Video-chats offer remarkable convenience. People who cannot practically connect in person can sometimes do so online. This can lead to connections that would never otherwise happen.

Unfortunately, the interface of the technology removes some of the most essential features of human connection though: real eye-contact, syncing of breathing, touch, and shared environmental context. Too often, people feel uncomfortable and disconnected in video but do not realize the degree that the medium is involved in that feeling.

Video-chat styles include things like tech setup, background, and how (and whether) efforts are made to make-up for the missing in-person contextual features.

## Audio calling

Audio calls enable us to focus on speech and tone of voice without staring at a screen to see a video feed. The focus on audio in particular brings both advantages and downsides. 

One of the main questions in audio calls what other contexts the callers have and how aware they are of one anothers' contexts. Where are they and what else are they doing during the call?

## Text 

Whether online or in books or otherwise, text offers many advantages. Texts do not require coordinating in time. The writing and the reading usually do not coincide. Text can be carefully crafted and edited before sharing. Text is easy to organize, to search, and to translate and adapt. Unlike other media, it is easier with text to collaborate and track changes.

With text, readers can go at our own pace. We can skim, skip around, go back, pause, and otherwise take in writing in the flow that works well for us.

Because text lacks real tone of voice, we can interpret text with different tones. Text can describe other senses like visual imagery, but it can never be completely specific, so it leaves room for creative imagination.

Note that the points above apply both to lengthy books and to short text messaging or online chat. Of course, the writing styles differ drastically. See the article on [language style](Language.md) for more on that.

In text, the style questions aside from language include layout and format. Books have different styles for margins and for presentation of different sections. Digital files (like these CL articles themselves) are organized in various ways in directories and separate files and linked in different ways. And with computers, readers have more control over the styles of presentation of the text (font, size, layout, etc).

## Static illustrations

Visual illustrations can express things that text cannot. As the saying goes, a picture is worth a thousand words. As with text, we can take in illustrations at our own pace and flow.

Illustration style is a vast topic that we won't get into here.

## Audio and video recordings

Recordings offer a lot more details and can directly share understanding that cannot be expressed in words. Of course, unlike text, recordings necessarily have whatever tone of voice and visual content they have. These things are not left to our imagination. Also, we can somewhat control the pacing of listening and viewing by skipping around and changing playback speed, but we do not have anything like the flow control we have with reading text or looking at static images. The pacing and flow in recordings is mostly set by the authors and editors.

With audio specifically, we can use portable devices and listen while doing other things physically (exercising, chores, traveling, etc). Video makes it harder to do such actual multitasking. Note that *real* multitasking involves activities that do not compete for attention such as walking mostly unconsciously while listening consciously to a recording — we cannot actually multitask in terms of paying attention to many things all at once (the best we can do there is rapid attention shifting).

Style within recordings is a massive topic. Today's software programs for making and editing recordings offer zillions of stylistic choices.

## Interactive software programs

Engaging with ideas through interactive programs can bring perspectives no other media offers. Within a context that can use all sorts of other media, the user has more control over pacing and direction and even creative input.

Style within programs themselves is a vast topic with all sorts of factors depending on the type of program.

Today, software can also be used to translate ideas between media. Computers can automate the process of creating descriptions of video and audio into text and of generating audio and video from text.
