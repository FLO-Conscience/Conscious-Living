# Language styles

Language supports much of how we communicate and how we conceive of the world. With CL, we bring more awareness, consideration, and review of how we use language.

Language styles vary so greatly that we cannot come anywhere close to discussing all the factors here. This article serves as just a starting point for exploring. Consider the variety of language among children at play, science writing, legal documents, and religious ceremonies. There's street slang of various cities, archaic dialects, the regulated language of mathematics, the norms of TEDx presentations, business jargon, evolving memes and trends online, freestyle rap, Romantic poetry, modern poetry, futurism etc. etc. And *within* any overall context, styles vary greatly.

[Wikipedia: Variety (Linguistics)](https://en.wikipedia.org/wiki/Variety_(linguistics) (and its links) is a more formal review of the study of variations in language.

For formal writing meant for publication, there are norms explicitly described in style guides such as those listed at [Wikipedia: List of Style Guides](https://en.wikipedia.org/wiki/List_of_style_guides). Those guides prescribe particular styles for particular contexts. Reviewing and questioning such recommendations is one way to think critically and consciously about language style.

## Style(s) of these CL articles themselves

This writing itself has a particular style. Is it the best for this context? Maybe there cannot be a single best style. Besides different readers, even a single reader will get different things from the same ideas presented in different styles.

For now, this writing emphasizes expressing the concepts thoroughly enough. The style itself may get updated later, and feedback is always welcome.

Maybe we can somehow end up with multiple documents **expressing the same ideas in multiple styles!** Could some technical set up enable practical connection of concepts across multiple style versions? As ideas get updated, we would want all the styles to stay coordinated. Perhaps a Large-Language-Model (LLM) computer program could eventually present a range of selectable style variations on the fly. Of course, while reading, we can do this ourselves in our minds. We can consider alternative styles, continually and intentionally seeing ideas from different perspectives. When an alternate style feels clearly better, we can contribute improvements. But no matter the style presented, we can stay conscious of the range of possibilities, of how no one style can express everything about an idea.

*TASK:* finish and publish first draft of a style-guide for this FLO-CL project itself

## Attitudes about language and language evolution

Appropriate, healthy attitudes toward language include recognizing its limits. Language is an abstraction. Raw experience is ineffable (beyond language). Conscious language includes accepting that it evolves and that we can thoughtfully engage in its evolution. We can choose how and when to change and adapt language or to *maintain* and stabilize it.

We need no ideological stances on language such as being strictly descriptive or prescriptive. Whether and when our attitudes are healthy and appropriate is a matter to investigate with the CL process (i.e. attune-resolve-review etc). We cannot say that one attitude is always right or wrong or helpful or unhelpful.

Well-established approaches to language include field linguistics, speech pathology, creative writing, public speaking, and many others, not to mention different schools of thought within those. These different ways of looking at language include philosophically contradictory perspectives.

When *playing* with language styles, we can easily to find places where languages break, fail, or are incomplete. There's bound to be combinations or variations of styles lacking in any language. When we find a missing aspect of a language, we can choose whether to develop new words, new uses of words, and new grammar and forms. Whether changes to language are appropriate depends on the effectiveness for the context.

## Different languages

Languages around the world have their differing patterns, norms, and cultural approaches. When feasible, language choice is itself a stylistic consideration. Mixing and borrowing across languages brings in another source of adaptation.

Cross-language linguistic insights can help us see new perspectives. For example, many Australian languages tend to categorize things very differently from Indo-European and even related languages. Exploring different languages can help us see the limits of specific languages and of language overall. Can we imagine linguistic concepts outside of any language we know?

At least initially, these CL documents are all in English — primarily because the working authors primarily use English.

## Translation styles

Translation brings up complex stylistic choices. Conscious translation means thoughtfully deciding which aspects of language to prioritize. We often care most about making the details of a story accessible, but we could prioritize style retention. However we choose to translate, *consciously noting* style differences helps us to better understand different languages and to consider style options.

Translators typically discard style differences and make different versions fit the norms of each language. For example, the Spanish "me gusta este estilo" is almost always translated as "I like this style". But Spanish has no verb for liking. Retaining more of the Spanish style: "this style pleases me". Both translations work, but the "I like" version *hides* the language difference. Contrast two teaching approaches: "Spanish for 'I like this' is 'me gusta este'" versus "In Spanish, we don't say 'I like this', we say 'this pleases me'".

Some languages have styles and concepts that really resist translation — other languages have no clear way to express the point at all. In other cases, languages may share style options but still have differences in which styles are used most.

We can compare translation questions to code-switching within a language. Do we change styles for different audiences? Do we read the original Shakespearean English or contemporary adaptations? When speaking with others who use a different habitual dialect, do we accommodate and adapt to fit in?

## Written vs spoken language

Writing and speaking have different properties. Some language structures are shown in writing while others remain unspecified. Some structures are clear within speech and others are open to interpretation.

Written language is often accompanied by diagrams or symbols or pictures which are hard to convey in speech, whereas spoken language includes tone of voice and other characteristics that are hard to convey in writing. We often think of writing as an encoding of speech, especially in contexts like stories, essays, and fables. It is also possible — and insightful — to examine a perspective where writing and speech serve more different (and perhaps complementary) purposes.

For various reasons, we tend to speak and write differently (and listen and read differently), even using different wording and grammar. Still, we can explore writing in a more spoken-word style and vice-versa.

Note that this article focuses on styles around word choices and word structures. Aspects of speech such as tone of voice and visual style of writing (layout, font, etc) are separate style topics that influence language but are not part of it.

A special case of written vs spoken language is *onomatopoeia* — words that just represent sounds (thud, thunk, bang, sklurtch…). When we read or speak about sounds, do we think of the actual sounds being represented or just the onomatopoeias? When reading aloud "quack" and "bark", do we pronounce the phonemes as written or do our best impression of duck and dog sounds?

## Language delivery and flow

In spoken language, delivery is largely about tone and timing. Importantly, listeners usually have limited capacity to anticipate the next parts and know the context of the current words. Sometimes, the rough amount of time for the whole message is at least understood. Speakers can also state at the beginning what will be covered.

In prepared written language (like this article), readers can usually know where they are in a sentence, in a paragraph, and in the overall work, and we can glance ahead to have a sense of what is coming up next.

In some contexts like text chatting and videos that show text, readers have less awareness of context and of what is coming. In extreme cases, writing can be presented while typed or otherwise letter-by-letter.

Some presentations combine written and spoken language, as in talks with slides or writing notes on a chalk board. In those cases, there are various styles in the amount of overlap between the writing and speaking and in the order of presentation (whether speaking before writing, writing before speaking, or both at once).

In various ways depending on the context, language can lead to anticipating one direction and then shifting in a surprising direction.

## Editing and planning vs improvisation

Whether speaking or writing, we can prepare more or less. We can pause to consider our language and then use careful, deliberate wordings. Or we can more freely speak or type-out our thoughts as we go. Either way, in writing or with recorded audio, we can then edit and revise later. In spoken conversation, we can explicitly "take back" something we say and restate it differently.

Along with the wide variety of styles among different people in patterns of langauage, differences will reliably arise between edited and/or carefully planned language and improvised language.

## Writing & speaking styles vs reading & listening styles

Though we focus here mostly on the styles within language content, we can also note distinct *reading* and *listening* styles.

Readers and listeners can interpret and emphasize language in various ways. We can take things literally or *read between the lines*. While taking things in, we can consider alternative language beyond what is written or spoken as a way to explore the messages. We can focus on accurately understanding the message versus on planning our replies or judgments. When reading, we bring in all the tone, timing, emphasis and more that written language leaves unspecified.

### Reading or listening with focus on responding

When we listen to someone speaking, how much do we focus on the context (including style) of their speech versus the content? How much do we consciously consider how we interpret? While listening, do we think just about understanding the message or about formulating our response?

Sometimes, we read in a context where we are *also* free to edit the writing. We may be reading our own writing (or something we have written collaboratively) or reading something in a FLO context where we can contribute edits. In those cases, how much do we read passively, just taking in the language, and how much do we actively look for ways to edit and improve? Each style of reading will bring a different mindset.

### Interpersonal language: greetings, acknowledgements, suggestions, feedback, questions, responses

What language styles do we use in particular with greeting others, asking for and giving comments and feedback, asking questions, and responding to these? What about bringing up topics or changing subjects?

Interpersonal dialogue styles are prominent in the way languages are often taught. We all want to know how to greet others, say goodbye, show thanks, ask for things, and so on. We also sometimes have complaints or feedback or questions for others and want to express them effectively. And when others have comments and questions for us, we want to reply appropriately.

So much of the way we handle these little linguistic interactions is done *unconsciously*. These contexts range from everyday experiences that we repeat countless times to unusual cases of connecting with someone we rarely encounter (and perhaps in an overall unfamiliar context). In the everyday cases, we can easily fall into unconscious habits and repeat rote styles that we picked up when we first learned language. In the unusual cases, we rarely get many opportunities to try different styles, review, and then iterate and update our approaches. Of course, we have in-between cases as well.

We need not accept the standard styles of these interactions as the only options. Exploring different styles can lead to more meaningful connections.

For example, instead of rote "you're welcome" or dismissive downplaying of gratitude with "it was nothing" or "no problem", we can consider thanking people in return as in, "thanks for your appreciation!"

All the style choices in this article apply to how we can consciously consider interpersonal language styles. We might find styles that feel best for our own general habits, but we might do well to adapt to each of the people in our lives and the styles that work best for each of them.

Sometimes, the best response to feedback is just to say, "point taken" and move on. Other times, a verbose reiteration showing our full understanding is more appropriate.

The CL commitments and practice resources include more suggestions and ideas for particular language for different contexts.

#### Conversation and turn-taking

In live conversation, we have different styles of taking turns. I.e. how do we navigate the shifts between writing/speaking and reading/listening? How readily do we interrupt (if ever)? What language do we use to interrupt? Do we ask to interrupt or just jump in? Do we check if someone is done before replying, such as asking, "is that all?" Do we use interjections to show listening such as "I hear you", "yeah", "really?", "go on", "say more", "tell me more", "wow", "ok", "sure", etc.?

Of course, exchange style differs in spoken conversation vs live-chat written conversation. In speech, only so much talking at the same time can happen and stay coherent. In text, the flow can get confusing with too much overlap, but all messages get through.

## Writing systems: alphabetic/syllabic vs logographic

Generally, writing comes in two versions: symbols that represent speech sounds (alphabetic roughly indicating phonemes or syllabic indicating full syllables) or symbols that represent concepts.

Logographic writing blurs into just representational art. It works as language when the symbols are somewhat standardized and reused in regular structures. Logographs need not be pronounceable or can correspond to totally different pronunciations (two spoken languages can share a system of logographic writing). New logographs are easier to introduce than new alphabetic letters.

A contemporary logographic system used alongside alphabets is *emoji*. How and whether we use emoji is an important style choice these days. 😼

## Written language formats

The various formats we use in writing provide many choices of ways to present the same content:

- Sentences
- Paragraphs
- Titles, headings, headlines
- Punctuation
    - including unrendered markup symbols
- Capitalization
    - note the semantic difference with "truth" and capital-T "Truth" and similar…
- Abbreviation
- Lists
    - unordered
    - ordered
    - nested or not
- Tables
- Illustrations and charts with labels and captions
- Parentheticals, sidenotes, footnotes, endnotes
    - and on computers: hovernotes, click-to-expand notes, encoded (unrendered) comments

## Writing forms

There are common forms that work like structural frames within which we put our writing. We can follow standards like five-paragraph essays or common forms of poetry. Usually, different contexts have norms. We use different forms and styles for emails, text messages, and blog articles.

Forms work as frames to provide some overall structure within which we craft our writing and track ideas while reading. Of course, as with any aspect of style, we can deviate from expected forms, and that will have its own effects.

Paragraph structure and other spacing and deliniation provide support for mentally *chunking* ideas. Despite norms and standards, the only real rule is that chunking ideas together will affect how we process language. We can explore all sorts of groupings and consider how well they fit our intentions. Within any chunk, we can consider where to put the focus, whether to lead into it or end with it or other arrangements.

### Poetry

Poetry (also called "verse") involves carefully structuring language around the sounds and rhythms of the words. Poetry often uses devices like rhyme, meter, assonance (vowel matching), alliteration (consonant matching) and more. Grammatical structures and semantics can also be rhythmically patterned such as with parallelism or contrasts. Poetry is usually written in stanzas with intentional line breaks.

Within poetry, there are numerous styles and forms.

Quality poetry can provide easier chunking for cognition, improved mnemonics (easier memorization), expressive strength, and engaging aesthetics (i.e. fun or inspiration). However, the challenges and limitations of poetry can lead to constrained or forced language. It also tends to take more time, energy, and skill to write good poetry compared to expressing the same overall points in prose.

We may associate poetic style with the topics we usually see in poetry, but we can use poetry with any subject. Outside of English, there exist traditions of using verse even for scientific texts and reference documents like thesauruses!

## Semantics

Semantics (meaning) in language includes questions like word choices among synonyms and similar.

One feature to especially note is semantic roles, also known as [thematic relations](https://en.wikipedia.org/wiki/Thematic_relation): theme (main focus), agent (deliberate actor), force (non-deliberate actor), experiencer (entity experiencing sensations and feelings), patient (what/who the action is on), direction/goal/recipient, purpose (motivation/reason), cause, instrument (means of action), manner, location, time, source/origin.

Details of *who* or *what* fills the roles goes beyond style and into the *content* of language. *Style* choices around semantics include how specific or general to be, which roles to state explicitly or to leave implicit, and which roles to highlight.

### On semantics of question words

We can interpret and answer questions in different styles. "Why" can mean what *caused* something or what was the *purpose* or *motivation* behind something. "When" and "where" can be considered in absolute terms or relative terms. Consider the Greek distinction between *chronos* (chronological time, time of day, calendar etc) and *kairos* (time as in situational context, appropriate or inappropriate time for something etc).

Often, questioners do not consciously consider which interpretation they intend. When we want to specify, we can consider can use other language to make the intention clearer. We can ask others which meanings they have in mind. We can also simply answer in the manner we find best — even if it goes against what we believe was intended. Sometimes, that shift can itself feel insightful. Of course, though often inappropriate, non-answers like "just because" and excuses are possible as well. There's also the style of responding to questions with questions…

### On language and identification

One concern, especially for CL, is the ways language can emphasize *identity* and *separation*. Words mark which things are this and not that. So much naming… When we describe *ourselves* in language, we tend to emphasize what *distinguishes* us from the rest of the world.

Throughout this discussion of language style and while exploring and playing with language, notice the *constrictive* nature of some styles. Identity is *inherently* constrictive and limiting. Of course, as emphasized throughout CL, constriction is not necessarily unhealthy or inappropriate. We can still take extra caution not to induce constriction with language when doing so does not serve a healthy resolution.

Language can play a role in *releasing* our attachments and constrictions. We might appreciate calling such shifts "disidentification". 

For example, "I am [Name]" emphasizes identification, whereas the passive styles "I am called…" or "I am named…" feel slightly less strong, provide some distance. We can more easily imagine being called or named something else than imagine *being* someone else. Active voice can work too, as in "I call myself…" or "They call me…" All of these still emphasize "I" and "me" in first-person identification. That language can shift as well.

Exploring language styles can bring options like: "I am angry" vs "I have anger" vs "anger flows through me" vs "there is anger" or "anger is here" or "noticing anger" or "is this anger?" and so many more variations.

Of course, beyond language style, we can flip stories around (changing the overall semantics). Consider this contrast from coach Jim Dethmer: "people tighten their grip on their identity" vs "the identity is tightening its grip on them".

When we consider the place people have in a context, do we label them *as* their roles (like "user", "consumer", "customer", "citizen") or with verb clauses like "people who…" or "those who…" (like "those who live here" or "people living here")? And when we do use role-labels, which do we use or emphasize? Does it change the way we think when we describe "user experience" or "customer experience" vs avoiding roles by talking about "human experience"?

## Grammatical style

Much of language style is in choices of different grammatical constructions. Grammatical choices include tense, case, aspect, person, number, gender, mood, and more.

Of course, grammar in practice is much more nuanced and complex than *any* guide can describe — just as *the world* is more nuanced and complex than *any* language can describe… Much of the study of linguistics is in finding, studying, and *laughing* about the quirks, breaking-points, inconsistencies, and craziness in language.

Although many overall points can apply to other languages, this article discusses English specifically.

Note that traditional English grammar ideas have a tragic history. For centuries, scholars and teachers described English grammar *as if it were Latin*. But English lacks many of the distinct conjugations and forms of Latin, does not follow Latin rules, and is grammatically Germanic. Where feasible, we work in this article to reduce the emphasis on Latin-based analysis.

Anyway, the descriptions here are meant to prompt conscious and creative exploration of style — not to define rigid grammatical categories. There are seemingly endless exceptions and qualifications, and we can never really define language absolutely — especially since it changes so readily.

### Syntax

Much of language style is in the syntax, the structure of language. Saying the same overall idea in different syntax will feel different. That such changes have meaningful effects shows how syntax and semantics influence one another. Syntax is not mere practical implementation.

Languages have implicit rules of syntax within which we can explore options. If rules we break, can go from quirky to hilarious, from to awkward nonsense. That said, syntax is more malleable than people often think. With repeated exposure, we can accommodate new constructions that felt wrong initially.

### Sentence structures

Different sentence structures:

- Simple: one independent clause
- Complex: includes a dependent clause such as "when considering sentence structure,"
- Compound: tying together multiple independent clauses that could be separate sentences
- Compound and complex

Clauses can themselves be longer or shorter, and compound and complex sentences can have different numbers of clauses. Although run-on sentences might worth avoiding, they are appropriate when they achieve effects that we want for whatever reasons arise in all the many situations within which we make decisions about language style — which we can do with conscious attuning and reviewing rather than with fixed rules.

### On incomplete clauses 

Incompleteness can work as a style in various media. Besides obvious examples like lists and notes, conversational language commonly makes use of incomplete clauses. We might say or write things like: "hearing you", "and?", "seems okay to me", "but the agreement…!"

We can start a sentence and leave it to the readers to…
That sort of style makes for interesting interplay in communication. We can invite others to complete the ideas.

We can also use complete sentences while stating that there is more left unsaid. We can do that by using ellipses or terms like "etc.", and "and so on", and others… These can feel noisy, but sometimes we want to prompt people to continue thinking further or to at least to feel that the idea is complete with nothing more to say…

We might also have incompleteness when we feel undecided about the best way to express something and prefer to just leave it unfinished and unresolved. We also might choose to share draft work in progress…

Incomplete clauses commonly fit conversation and thinking when the context can be implied. "If you finish that project, then…" "*When* not if!"

We can leave out the subject to reduce emphasis on it, as in meditating and noting "hearing sounds" instead of "I am hearing sounds".

Incompleteness is also useful as a practice for disrupting discursive thinking via an intentional thought progression like: "Who am I?", "Who am", "Who", "…"

### Language moods

Linguists typically consider distinct moods as present in a language only when they use specific grammatical structures like changing word morphology. For our goal of conscious language style, any mood that leads to trying different wordings is useful to note.

- Declarative/Indicative (plain statements)
- Interrogative (questions)
- Imperative (direct instructions, commands)
    - Prohibitive (negative imperatives)
    - Hortative/Cohortative (assertive invitation, suggestion)
    - Benedictive (polite requests)
- Conditional (if-when/then, would-if)
- Subjunctive (rhetorical, imaginary)
- Hypothetical (speculation, possibility)
- Dubitative (doubting, uncertain)
- Optative (wishes, hopes, prayers)
- Desiderative (desire and wanting)
- Permissive
- Jussive (expressing judgment)
- Potential
- Presumptive

The same instructions expressed in different moods:

- Declarative: "As you attend to present moment, you allow things to be just as they are."
- Imperative: "Attend to the present moment, and allow things to be just as they are."
    - Prohibitive: "Stop focusing on the past or future, and stop insisting that things be different from how they are."
    - Hortative: "I suggest you attend to the present moment and then allow things to be just as they are."
    - Cohortative: "Let's attend to the present moment and allow things to be just as they are."
    - Benedictive: "Would you please attend to the present moment and allow things to be just as they are?"
- Interrogative: "As you attend to present moment, can you allow things to be just as they are?"
- Conditional: "If/when you attend to present moment, then you can allow things to be just as they are."
- Subjunctive: "If you were to attend to present moment, you could allow things to be just as they are."
- Hypothetical: "If/when you attend to present moment, you might allow things to be just as they are."
- Dubitative: "I guess when you attend to the present moment, maybe you allow things to be just as they are."
- Optative: "May you attend to the present moment and allow things to be just as they are." 
- Desiderative: "I want you to attend to the present moment and allow things to be just as they are."
- Permissive: "You may attend to the present moment and allow things to be just as they are."
- Jussive: "You should attend to the present moment and allow things to be just as they are."
- Potential: "You could attend to the present moment and then allow things to be just as they are."
- Presumptive: "Assuming that you're attending to the present moment, surely you allow things to be just as they are."

Notably, a simple question mark (or analogous spoken prosody) can shift imperative wordings into hortative (suggestive) mood.

Note how conditional statements have different implications depending on the use of "when" or "if". *When* asserts more confidence that something will indeed happen. With healthy topics, "when" can feel more reassuring. With risks, "when" tends to feel more sense of inevitability which might inspire preparation or trepidation depending on our mindset.

Many of the moods described here come from consideration of styles in other languages. As another example, though we are unsure of the translation, the linguistic idea of "mirativity" might imply another intriguing-surprise sort of mood, as in: "How about that‽ You're staying present and allowing things to just be!" We could yet explore and describe many more moods. The ones here are not an exhaustive set.

All these moods can be *mixed* too. There's a sort of presumptive-imperative-desiderative in an instructor saying, "next, you'll want to…"

### Tense and aspect

How do we consider the time perspective of statements? Past, present, future, and other variations… do, are doing, did, have done, had done, have been doing, had been doing, will, will be doing, will have been doing… 

### Personal perspectives

We have options for "grammatical person" (and **singular vs plural** within that):

- First person (I/me, we/us)
- Second person (you, y'all)
- Third person (they/them, he/him, she/her, it)
    - also proper nouns
    - and specifically identified common nouns, such as when marked with the/this/that/these/those 
- Impersonal (one, people)
    - and common nouns when not specifically identified

Note that "you" and "we" can be used *generically* to function impersonally. There's also a sort of *generic-they* implied by some particular forms like "they say that…"

It can sometimes be unclear whether we're using generic, impersonal perspective. The style can feel like projection (implying that indeed *you* the specific audience or *we* including the audience do indeed fit a stated description). Even when intended generically, it's easy to take such language personally. To clarify impersonal explicitly, we sometimes need to actually state something like, "just saying this about people in general, not anyone in particular…" But even then, *some* people may still interpret *that* personally, imagining that the language is being used only for plausible-deniability.

Some languages have *clusivity* distinctions for first-person plural — different styles denoting whether the audience is included or excluded. Incidentally, in CL writing, *we* use "we" mostly, and the clusivity must be interpreted from the context. When "we" is about the writing, it implies exclusivity (to the authors), but we mean "we" inclusively in most contexts about how people may think, feel, or act. Sometimes, our "we" is inclusive of the authors and readers in particular. Sometimes it is more of the *generic we*.

Among other implications, the perspective affects our choices of pronouns and possessives.

#### Indefinite pronouns

Besides the standard personal (and impersonal) pronouns, *indefinite* pronouns provide more ways to express *general* descriptions without reference to *particular* people or things. Examples include the combinations of any-, every-, some-, and no- with one, body, thing, time, and where. Another set uses -ever as in forever, never, and with the question words: whatever, whoever, whichever, wherever, and however ("whyever" is non-standard). Other indefinite pronouns include: enough, little, less, much, more, most, plenty, one, several, few, many, some, none, all, such, both, either, neither. [Wikipedia: Indefinite Pronoun](https://en.wikipedia.org/wiki/Indefinite_pronoun) has a good chart and longer description.

#### Possessives

Posessives are inherently constrictive. Using them consciously can show us ways to get more detached perspectives. Consider these variations: "my foot hurts" vs "the foot hurts" vs "I feel pain in my foot" vs "I feel pain in the foot" vs a no-subject phrase "feeling foot pain" (among many other options).

### Linking-verbs: "be" and alternatives

English uses the verb "be" and some other alternatives to connect subjects and complements (adjectives or nouns that describe the subject). We can take the phrase "the formal linguistics term *copula*" and make it a full clause: "The formal linguistics term *is* copula" (copula *means* linking).

Some other examples: "fear was present", "craving is constrictive", "we are here"… This X-is-Y form is the main type of linking-verb construction.

One way to explore linking-verb style is reversing the subject and complement. Reversing the subject and complement is one way to explore linking-verb style. Some reversals are unremarkable, but others can feel more startling. Compare: "Raw experience is ineffable" vs "Ineffable is raw experience". Some reversals change meaning too much, so they don't always work.

One approach to language style is avoiding "be" (and its conjugations). Besides pushing us to find alternative language (which feel more expressive), reducing use of "be" assists with translation because the complexity of "be" presents particular difficulty there.

Here are some alternative linking verbs which also work in the form "subject [link] complement" (some work with only adjectives, others with only nouns, some work with both):

act, appear, become, bleed, breathe, call in, come, come out, constitute, die, emerge, end up, equal, fall, feel, get, go, grow, keep, lean, leave, lie, look, mean, play, plead, prove, remain, run, seem, show up, smell, sound, start, start out, stay, stand, taste, test, turn, turn out, turn up

The alternative linking verbs tend to better emphasize process or subjective interpretation.

What makes these distinct from other subject-verb-object structures? Linking verbs don't do an action to a patient or recipient. The linking-verb clause "slow breathing feels calming" does not mean that breathing is doing the feeling of something called "calming". Contrast with non-linking-verb: "Slow breathing calms us".

Of course, we can express the same overall ideas uses with other structures that do not use linking verbs.

A related pattern but without linking-verbs: *nominal* clauses like "we believe it appropriate" which leaves the linking "is" only *implied* (the explicit version being "we believe that it is appropriate").

Incidentally, a related style choice to note: whether to include or omit "that" and "which"… Also, notice there how we can use punctuation (or appropriate spoken prosody) as a substitute for linking verbs.

#### As as a be-substitute

As an introductory clause, the form "as a/an [ ]" works as an alternate style for contexts that might use linking verbs with noun complements.

We can change "I am an explorer, and…" to "as an explorer, I…"

A variant with reduced sense of identity: "From an explorer's perspective," or moreso, "from the perspective of exploration,"

### Active or passive voice

Active voice means the actor is the subject as in "we read the article." Of course, the *theme* might still be the object — that depends on the emphasis and context. To *really* focus on the actor, we could use an unusually emphatic style: "It was you who…"

By contrast, passive voice puts the focus on the action and its patient. Thus, focus on identity and agency is reduced, especially when the actor is left unspecified. "The article was read."

Note that some languages have a specific passive conjugation. English has no such option. What we call "passive voice" is made with a linking-verb construction (usually with "be" or "get") and a past participle as the adjective complement.

Incidentally, some of the alternative linking-verbs can work, but most make the participle verb more clearly functioning as an adjective. "Their attitude was transformed" implies a missing actor/event, as in the active statement "the review transfomed their attitude". But compare with "their attitude emerged transformed" which brings more emphasis to just the state of the result.

Some other passive constructions: *bare passives* (without the linking-verb) work as introductory clauses as in "With the meditation finished, I went to sleep" and following "have" in structures like "we have the group led by a facilitator".

Also, the verb "happen" provides a passive framing. We can say that something "happens" while saying nothing about any actor that made it happen.

#### Labile verbs

[Labile verbs](https://en.wikipedia.org/wiki/Labile_verb) offer another way to emphasize action and object rather than actor. They work with transitive verbs that can also function intransitively, leaving out the actor and putting the patient as the subject. Some labile verbs need adverbs in order to work this way.

We can say, "the article reads well", and we don't mean the article is doing the reading. Some cases are ambiguous in implying reflexivity or passivity. We could interpret "the mood shifted" as if the mood shifted itself somehow or that an unstated event did the shifting.

Some people call this "middle voice" or "mediopassive voice" because of the similarity to passive voice.

### Reflexive language (-self)

Reflexive language emphasizes an internal rather than external focus. It shows up when the actor is also the recipient of an action. English has reflexive pronouns using the term "self": myself, ourselves, yourself, yourselves, itself, themselves, oneself… We also have the related *reciprocal* phrase "each other".

We can use reflexive language in interesting ways to differently emphasize or define roles. Reflexive can sometimes work as an alternative to intransitive. Notice the difference with the instructions "relax" vs "relax yourself" or "focus" vs "focus yourself".

Saying, "we attune first to ourselves" emphasizes the *self* as a concept because it implies that there are other things outside of ourselves. We could instead say, "we attune first to our thoughts and feelings" or drop the "we" and "our" entirely with imperative mood: "Attune first to thoughts and feelings."

Another reflexive example, consider this startling progression playing with language style: "You hurt me", "your words hurt me", "I felt hurt by your words", "I heard your words, and I felt hurt", "I hurt myself with your words".

Some languages use much more robust reflexive grammar than English, see [Wikipedia: Reflexive Verb](https://en.wikipedia.org/wiki/Reflexive_verb)

### Use of grammatical word classes

Common categories of words include nouns, pronouns, verbs, adjectives, adverbs, prepositions, auxiliaries, conjunctions, interjections, and determiners (articles, demonstratives, interrogatives, possessives, quantifiers).

One way to adjust style is to use of more or less of different word classes. For example, people explore writing that avoids adverbs or adjectives. Nouns tend to have a *fixed*, static quality compared with the way verbs emphasize time, process, and change. What happens if we focus on using more verbs and avoiding nouns?

#### Converting word classes

In English, we can shift all sorts of words across classes.

**Affixes** (prefixes and suffixes) often (but do not always) play a key part in word-class conversion.

Noun and adjectives become verbs with affixes like -ize/-ise, -ify, -ate, en-, be-, de-, un-, and re-. We can also simply use nouns directly as verbs. When we do this sort of conversion, do we verbize, verbify, enverb, beverb, or simply *verb* other words? (all those can be understood, but "verbify" and "verb" are the two in actual use). Note that, in CL writing, we usually prefer coulding over shoulding.

We can form adjectives from other classes with affixes like -ish, -al/ial, -en, -ate, -ative, -etic, -ic, -ical, -ous, -ary, -ful, -less, -able/ible, -y, and -like. And past participle (usually -ed) and progressive (-ing) verbs work as adjectives.

Most adverbs are just adjectives with the suffix -ly. Prepositional phrases can also work like adverbs.

Progressive (-ing) verbs can work as prepositions.

All words can be used as nouns if the meaning is the *word* itself (quotation marks make it more explicit though), though sometimes the meaning can also carry through, as in "feeling a lot of *go* right now…" But the *normal* way to noun words is with suffixes: -ness, -ity, -tion/sion, -ment, -ance/ence, -ism/ist, -dom, -hood, and (for those involved in an action) -er/or/ee.

Incidentaly, consider the common style of describing ideology and identity labels with -isms and -ists. These labels may imply fixed mindsets and can tend to trigger prejudices and assumptions. We might label someone or something "-ist" and find ourselves imagining simplistic stereotypes. A style shift can disrupt that pattern. For example, we can try alternative wordings using -(i)an or -ic adjective form. "Stoic" describes an attitude whereas "stoicism" an ideology. What about novel forms such as the -ish suffix which imply fuzziness (consider Marxish, Buddhish)?

Overall, word-class conversion opens up style possibilities in two ways. We can have a word in mind but express it with different grammar. Or we can have some grammar in mind and realize that we can borrow words from other classes rather than feel limited to words we more habitually think of for that context.

## Vocabulary and word choices

Within discussion of grammatical styles and elsewhere in CL, we include lists of synonyms and related words. Conscious word choice has all sorts of subtle and remarkable effects on our stories and understandings.

To expand our vocabulary options, we can use dictionaries and thesauruses and consciously note interesting language whenever we encounter it.

Nouns, verbs, and adjectives fill the bulk of vocabulary. Some categories have very few options. For articles, we only have a/an, the, some, any. For demonstratives: this/these, that/those.

### Interrogatives

When asking questions, do we ask who, what, when, where, why, how, or which? And in what *order*?

### Auxiliaries and similar

Auxiliaries include can, could, dare, do, may, might, must, need, ought, shall, should, will, and would. These also all work as negatives with "not".

The prevalence of "should" in the context of advice and instructions should be a source of constriction (do you notice some constriction here when being told that you *should* feel constricted?). Some people talk of "shoulding" as a behavior to consider avoiding. "Would" can seem more neutral, but it still goes beyond saying that something is possible. "Would" implies that something is *supposed* to happen. "Can" and "could" offer possibilities — bringing a sense of *suggestion* only to the extent that mentioning an option at all brings attention to it. We can also soften strong assertions like "this helps" by saying "this could help" or "this might help".

"Do" has some unusual uses in English that make it distinct from the other auxiliaries, see [Wikipedia: Do-support](https://en.wikipedia.org/wiki/Do-support).

Some alternative constructions which function similarly to auxiliaries:

- so-called semi-auxiliary phrases
    - using would: would sooner, would as soon, would rather
    - using had: had better, had best
    - using be (and its tenses): be about to, be able to, be going to, be likely to, be supposed to
    - ought to, used to

- Catenative verbs (which work in all tenses, unlike auxiliaries) such as: have to, need to, want to, deserve to, get to, like to, care to, and many more
    - [Wiktionary Appendix: Catenative Verbs](https://en.wiktionary.org/wiki/Appendix:English_catenative_verbs) has a full collection to explore

- "it's \[ ] to" as in "it's healthy to" which has a similar sentiment as "should" but with more nuance and more variations available

<details>
<summary>
"it's [ ] to" usage and vocab options [click to expand]
</summary>

#### Details of the "it's [ ] to" style

The quirks of this phrase structure serve as a good example of how nuanced and strange language can be. We can explore the variations for their own value as well as considering that many other situations will have their own comparable complexities.

This "it's" construction mostly makes impersonal general assertions. We can bring in more personal perspective by qualifying with language like "to me" or "for me" as in "it is healthy for me to" or "it feels healthy to me to". This works with any personal perspective — first, second, or third. It also works with impersonal "one" or "people", though that's redundant since the plain "it's [ ] to" is already impersonal.

Interestingly, to use names or personal pronouns instead of "it" requires that we use a *different* set of description words. "We are [ ] to" goes with "we are glad to" or "we are willing to", but isn't normal English to say "we are healthy to…" nor to say "it's willing to…". There's something about the adjectives that need to match the general vs personal here.

We do not have a clean way to determine or describe which adjectives fit this structure overall. Common feeling words like "sad", "angry", "happy" all work with the *personal* style ("he was angry to see…"). But, strangely, of those, *only* "sad" works normally with the impersonal. It feels unremarkable to say "it's sad to see…" but contrast that with "it's angry to see…" or "it's happy to see…"

Have linguists labeled or described which adjectives have the relevant traits to work with "it's [ ] to"? We don't know as of this writing. For now, we just will use example vocabulary to help us consider this particular construction in exploring our attitudes, judgments, and perspectives.

The full impersonal syntax here is: "it [linking-verb] [description] to [verb]" as in "it's healthy to review" or "it was tedious to put together". It also works with the reordering: "to [verb] [linking-verb] is [ ]" as in "to review is healthy".

Features of this style to note:

- works in all tenses
- works with *some* of the linking-verbs: appear, be, become, end up, feel, get, look, prove, remain, seem, smell, sound, start out, stay, taste
    - which may themselves be preceded by *some* of the auxiliaries: can, could, may, might, must, should, will, would

- the description can be an appropriate adjective; here's an incomplete list to explore:
    - good, better, best, great, bad, worse, beneficial, valuable, worth it, relaxing, calming, wonderful, awesome, amazing, energizing, enjoyable, envigorating, exciting, epic, extreme, splendid, (un)cool, (un)acceptable, ok, alright, fine, fun, funny, silly, ridiculous, ludicrous, absurd, crazy, quixotic, nice, nasty, awful, annoying, upsetting, disgusting, sick, sickening, horrible, terrible, boring, bothersome, tiresome, tiring, tedious, mundane, (un)remarkable, (un)pleasant, (un)comfortable, comforting, miserable, detestable, stressful, distressing, disturbing, depressing, (dis)empowering, (un)helpful, (un)healthy, (in)appropriate, (un)necessary, strategic, safe, dangerous, reckless, scary, nerve wracking, unnerving, risky, (in)sensitive, (un)sound, (in)correct, (in)effective, (im)practical, pragmatic, right, wrong, critical, severe, careless, (im)prudent, ignorant, humble, arrogant, (im)modest, (un)reasonable, (un)charitable, (un)generous, (un)fair, (dis)honest, (in)sincere, erroneous, mistaken, shallow, profound, important, essential, foolish, pointless, sloppy tragic, (in)considerate, thoughtful, thoughtless, hasty, rash, (un)biased,(counter)intuitive, (un)satisfactory, (un)satisfying, (in)sufficient, (in)effective, (un)kind, (un)loving, (un)friendly, (in)humane, hateful, mean, rude, (un)compassionate, (un)merciful, (ir)responsible, dutiful, (un)ethical, (im)moral, (im)polite, (un)tactful, (un)courteous, (un)civil, (dis)respectful, (dis)graceful, (un)selfish, selfless, sad, humbling, (un)fortunate, (in)sane, (in)tolerable, (un)constructive, destructive, disruptive, stifling, hypocritical, (in)consistent, confusing, stupid, smart, (un)skillful, (un)wise, (ig)noble, (un)holy, (in)decent, (un)just, (un)lawful, (il)legal, (il)legitimate, (im)proper, (un)fitting, (un)natural, shameful, shameless, (dis)honorable, punishing, painful, painless, hurtful, insulting, awkward, problematic, troublesome, (in)consequential, trivial, easy, hard, difficult, challenging, simple, complex, (in)formal, casual, customary, standard, strange, weird, odd, (ab)normal, (a)typical, (extra)ordinary, (un)usual, (im)possible, *and many others even though many adjectives do not fit…*

- the description can also be a noun (or noun phrase with its own added adjectives) if used along with an article or possesive (most commonly "a/an" or "my/our"), such as "it's a special honor to" 
    - with nouns, *some* of the linking-verbs need an extra "like" as in "it feels like a problem to" though others do not, such as "it remains a problem to"
    - nouns for this context include: treat, gift, joy, honor, privilege, mitzvah, good deed, duty, responsibility, job, way, style, trend, norm, intention, idea, desire, pleasure, mistake, error, problem, challenge, shame, disgrace, pity, pain, bore, insult, danger, risk, (im)possibility, chance, choice, *and others*
    - a few nouns (and noun phrases) work either this way or without any article or possesive: custom, tradition, standard, time
    - certain nouns only work as noun-phrases with an adjective, usually "good" or "bad" (or related), and usually *without* an article or possessive: practice, manners, style, taste, luck
        - also the particular phrase "hard work"
        - also *skill*-related nouns with -ship like musicianship, scholarship, leadership (but not other -ship nouns)

- both the adjectives and nouns here can work with modifiers like "not", "less", "more" (or "-er"), "so" (for adjectives), "such" (for nouns), and other adverbs

</details>

### Prepositions

Prepositions express different relations and perspectives. Consider "it happened to me", "it happened for me", or "it happened through me". 

Preposition changes tend to alter the core meaning, to change the story. Still, exploring prepositions can be useful even when we want to keep the core story unchanged. When we want to keep the overall story content, we can adjust *other* words around different prepositions.

Common English prepositions to consider:

aboard, about, above, across, after, against, along, alongside, amid, among, around, as, at, away (from), before, behind, below, beneath, beside, between, beyond, by, down (from), during, for, from, in, inside, into, near, next (to), of, off, on, onto, opposite, out (of), outside, over, past, since, through, to, toward, under, underneath, up (from), versus, via, with, within, without

See [Wiktionary: English Prepositions](https://en.wiktionary.org/wiki/Category:English_prepositions) for a more exhaustive list.

### Positive vs negative language

Although correlated with constriction and expansion, we can use positive or negative language styles in any context. A common suggestion is to use more positive language by sticking to **"yes-and" instead of "but"** (or synonyms like however, yet, still, and others). Whether we stick to that suggestion or not, we can consciously consider this aspect of our style.

We can also consider how we use double-negatives like "not uncommon", "don't stop", and many others. How do those compare to alternative positive wordings?

We can play with positive and negative affixes like pro-, anti-, il-, im-, ir-, dis-, in-, un-, ex-

Besides examples which are literally positive or negative (good/bad, yes/no), some language has positive or negative *connotations*. For example, when describing common vs uncommon things, some words have more negative connotations: abnormal, strange, weird, deviant…; other terms are more nuanced or neutral: unusual, odd, peculiar, quirky, rare, different, atypical, unconventional,  divergent…; and others have generally positive connotations: extraordinary, exceptional, special, stand-out…

### Affixes

Besides the medium-sized list of *grammatical* prefixes and suffixes mentioned in other sections here, there are numerous [affixes](https://en.wikipedia.org/wiki/Affix) that are more semantic than grammatical… They include many terms from science and medicine as well as quirky slang and so much more.

### Extremes vs moderation

Do we use the certainty of hyperbole and extremes like always, never, all, none, everyone/everybody, noone/nobody, everything, nothing, and superlatives (most/least)?

Or do we use more nuanced language like often/sometimes/rarely, many/some/few, much/little, and comparatives (more/less)?

### Certainty vs cautious language

Do we use language like absolutely, sure, certain, definite, no doubt, indisputable, undeniable, obvious, settled, no debate, proven? Do we state assertions boldly without qualification?

Or do we use cautious hedging language like maybe, likely, probably, possibly, arguably, unsettled, or uncertain?

We can qualify statements by making the language and thought process explicit. Compare "there is X and Y" vs "we can think of things as either X or Y". This is similar to metaphor vs simile, explicitly saying "X is like Y" versus "X is Y".

Sometimes, hedging and qualifying can be noisy and unhelpful, but it can be appropriate in some cases. Consider qualifiers like "I think," "IMO", "it seems", "I imagine", "one story is", "I heard that", "what about/if…?", and so on.

### Language of traits or types, spectra or discreteness

When describing something or someone, do we use "be" or "have"? When we talk of who someone *is* or what something *is*, that encourages thinking in types. By contrast, describing the characteristics someone or something *has* gets us to think more about traits.

Also, instead of all-or-nothing, we can emphasize ranges in categories by describing being or having *more* or *less* of (or being/having *weak*/*strong* in) a type or trait.

### Plain language and basic vocabulary

Plain language means simpler syntax with limited, basic vocabulary. Even complex ideas can often be expressed plainly. For a good reference and example of plain language, see the [Simple English Wikipedia](https://simple.wikipedia.org).

### Vernacular, slang, casual styles

All sorts of dialects offer their own stylistic variations. Vernacular and slang include a wealth of great expressive styles. Besides appreciating their particular stylistic traits, vernacular language can be more accessible. That said, cartoonish sloppy use of vernacular usually goes badly. And  misunderstandings can arise when using a dialect outside of its normal context.

Countless discussions and examples of vernacular style exist. One notable example is the roughly 4,000 [English interjections included in Wiktionary](https://en.wiktionary.org/wiki/Category:English_interjections).

In writing, we can use non-standard spellings. One example: quirky spelling trends in text chat. Writing meant to represent spoken word styles sometimes uses non-standard phonetic spelling to indicate accent (this is called *eye-dialect*).

### Filler words 

Common filler words include "like", "y'know", "I mean", "okay", "so", "well", "right?", and others. Non-word examples include "um" and "hmm".

Although filler words are more common in spoken language, we can also use them in writing to express a casual feel closer to speaking.

The filler words can be used more meaningfully in appropriate contexts. Do they retain some semantic significance even as fillers? They do seem to have some real implications. "Like" can sometimes mean "for example" in a manner emphasizing that what follows is an *impression* or a *rough* thought that the speaker isn't yet sure about. "Y'know" and "right?" can work as checks like, "you with me?" or "that make sense?" And the others all communicate some things as well (beyond merely the stereotype that the speaker is nervous).

Related filler terms include prefacing like "no offense," "simple," "just," "easy," "honestly," "actually," "basically," "briefly," and more. Another example is answering questions with "good question," And many less-common fillers can become habits for individuals or within small groups.

So, do filler words have a healthy place in language? We can explore this question consciously and practice being intentional with our use of these patterns. The main issue with filler words is the *unconscious* use and *excessive* use of them.

### Ancient or foreign terms and specialized jargon

Using obscure, specialized terms enables us to set the definitions more precisely and avoid the baggage of prior impressions and connotations. Similar patterns apply to using foreign or ancient terms that are unfamiliar to audiences today.

Jargon tends toward pedantic, rigid, and religious treatment of language.

Jargon is often exclusionary, blocking access for those unfamiliar with the terms. Some types of specialized language have been called out with terms like "academese", "medicalese", and "legalese".

For naive audiences, using jargon risks unhelpful mystical connotations, confusion, and overinterpretation. People often show *excessive* deference to specialized language and may not feel they have a place to question it.

People may intentionally use jargon with naive audiences as a way to get deference and/or to mask poor understanding of a topic. Critical terms like "psychobabble" and "technobabble" refer to that sort of inappropriate use of jargon.

Sometimes jargon matches or is similar to common terms, and then confusion can arise from contradictory definitions.

For better and worse, much jargon tends to *lack* metaphorical connections. We understand most ideas through metaphor, so we have a harder time understanding terms that trigger no conceptual connection to ideas from other contexts. Such disconnected terms give us a more neutral starting point for understanding, but they also are less accessible.

When using jargon, it can help to explicitly acknowledge it, as in saying, "in CL, we use this precise definition of the word 'agreement'…"

### Idioms

Languages have many quirky idioms, both in casual and formal contexts. Like jargon, idioms can be less accessible to those not familiar with them. We can bring conscious awareness to our use of idioms in order to use them appropriately. We can also play with idioms and deconstruct them to see them in new ways. 

### Clichés and buzzwords

Language that gets *overused* can lose its spark, its impact. Styles and terms that get really trendy can start to get used unconsciously and inappropriately. As metaphors get overused, they eventually lose their metaphorical effect and just become rote words or sayings.

To retain the value of language that has become cliché, we may need to put it in novel contexts or highlight it in careful, explicit ways. Maybe *acknowledging* the overuse itself can invite people to see it again with fresher perspective.

To avoid such language entirely, we can go in either of two directions: stick to more basic language or invent novel sayings as prior ones become stale.

### Abbreviations and shortening

Though some abbreviations are specialized jargon (like "CL"), others can be clear and casual. Slang often uses shortened words that still retain clear origins.

As a specific example, "appropriate" is a long, cumbersome term that is still *appropriate* in meaning. Common shortening processes would lead to saying "prope". That can look a bit like "proper" in writing, but the pronunciation of the vowel is different (and the meaning is close, though appropriate means fitting, good for the context whereas proper means following of rules and standards).

An important consideration for abbreviations: whether they risk being confused with other words. For example, some acronyms are spelled and pronounced the same as an everyday word. Those acronyms are harder to search for and are more easily confused.

### Significance of etymology

Etymology (the study of the evolution of words) can help inform language style choices. Often enough, contemporary language retains patterns from older language. Even when we don't know the etymology, we pick up on patterns. So, etymology has some influence whether or not we understand it or are conscious of it.

We can accept that words do evolve while also recognizing that our focus on etymology can become excessive. Appropriate amount of emphasis on etymology varies and can depend in part on how much connection to its history is retained in any particular word.

#### Terms with different linguistic roots

English uses a mix of words with origins mostly in French, Latin, Germanic (Old English, Old Norse), and Greek. Often, we can express the same overall ideas in multiple ways by choosing which word style to use. Examples:

- [Wikipedia: Paired list of Germanic and Latinate words in English](https://en.wikipedia.org/wiki/List_of_Germanic_and_Latinate_equivalents_in_English)
- [Wikipedia: Paired list of Old French and Old English words in English](https://en.wikipedia.org/wiki/List_of_English_words_with_dual_French_and_Old_English_variations)

### Using language "correctly"

Language evolves in part due to unconscious mistakes and confusion which then can become normal later. Still, in any particular context, words do have a range of commonly-understood meanings. Most other style features do not have such issues. We might unknowingly go against the norms for how to dress for a cultural context, but the only ways to dress truly *wrongly* involve practical errors like putting the left shoe on the right foot or wearing a big floppy robe for a bike ride. Most styles can be healthy or unhealthy for a context but not simply erroneous. We *can* use words *erroneously* such as mistaking a similar word for the one that means what we actually intend. How do we reduce such mistakes? How do we respond when we or others make errors?

### Some misc other vocabulary notes

Although often synonmyous, "sensible" and "reasonable" have distinct connotations. "Sensible" implies ideas that fit well with our patterns of direct sense experience, our *feelings*. "Reasonable" implies ideas that fit well together coherently, supported by well-considered *thoughts*.

People often plainly assert that something is good or bad or that we *need* something. We can make statements more clear and qualified by expressing that things are good, bad, or needed *for something*. Contrast "we need this" versus "we need this in order to…".

"Drivers" is an alternative neutral term for "needs" or "wants".

As an alternative to "expectations" and "hopes", we can use "predictions" and "aspirations". The former tend to come with constrictive feelings more than the latter which feel more open though still directional. Other related terms include "anticipate", "envision", "propose", and more.

The word "try" has different feelings and connotations than "do your best" or similar. A common suggestion is to avoid "try".

People often ask, "okay?" when they really mean "do you understand?" rather than "do you agree?"

In describing getting paid, many people say, "make money". What happens when we consider alternatives to "make" like: "earn", "get", "receive", "take", or other possibilities? What about other phrasings like "they pay me", and instead of "pay", options like "give". What about these wording options with other subjects, even abstract ones like "love"? Does changing the subject give us insights into how we feel about the verb choices?

## Accenting parts of language 

Language style includes whether to accent particular parts of language and choosing which parts. Emphasizing different words or phrases can greatly alter meanings and feelings. We usually do the actual accenting with non-language styles.

We can bring particular attention to parts of writing by changing the visual style in various ways:

- oblique/italic
- bold
- underline
- encircling
- strike-through
- black-out (full redaction, covering letters entirely)
- coloring (background or letters themselves)
- font change (design, visual effects, and/or size)
- separating with space

In speech, we can accent by:

- saying some things more slowly
- saying some things with different tone or loudness
- putting breaks (silence) around words
- distinctive gestures and facial expressions

Note the speech analogs for black-out censoring: covering one's mouth or coughing or inserting a beep or noise in recordings.

There are also directly-linguistic methods to bring emphasis. We can use [expletives](https://en.wikipedia.org/wiki/Expletive_(linguistics)) like "indeed", "get this…", and similar, as well as more vulgar options and expletive infixes. We can also more verbosely mark things with wordings like "here's the main point:" or "the key takeaway is:" and similar.

## Concision

How brief can we get and still communicate effectively? *Pithiness* can be a healthy style. We can aim for a high signal-to-noise ratio. Getting right to the main point can help reach more people quickly. We can *then* prompt for questions to see where to expand and clarify.

Excessive concision leads to oversimplification, missing nuances, and misunderstanding. Concision tends to reinforce preconceptions and conventional views because it reliably takes more time and words to explain unfamiliar, challenging, counterintuitive, and startling ideas and perspectives. To escape groupthink and echo chambers, we may need patience or persistence in our communication.

In all contexts, it is possible to express too much or too little. Pithiness means finding an appropriate and healthy quantity of words for each situation. To balance the issues, we can use headings, highlights, and other ways to make it easier to skim writings while still including details and nuance otherwise.

Knowing the audience is especially key here. Some audiences will be ready to jump right to a main point while others need more preparation. When making notes-to-self we might use only the tiniest shorthand to prompt later recall of ideas. For a potentially wide audience, we might carefully explain background ideas and explicitly list what topics someone should understand first before they continue.

## Repetition, redundancy, reiteration, and reinforcement

When does redundancy aid understanding? When does it just add noise? Is there an appropriate place for [verbosity](https://en.wikipedia.org/wiki/Verbosity)?

Any one way to express something will have its particular nuances and connotations. As we have said repeatedly here, alternative styles and language can help us see more perspectives. Instead of exploring options and picking just one preferred version, we can use *multiple* versions.

Throughout these CL articles, we have found it appropriate to list selected synonyms to help point at ideas from many angles and to offer flexibility.

Note that extended phrases with extra words can range from emphatic synonyms (like profoundly deep) to paradoxical and oxymoronic (like profoundly shallow or superficially deep). Each combination provides its own view. We can choose sets of views that we feel best frame the overall understanding we want.

Besides variations, plain old repetition also reinforces ideas. We can reread things or simply say things multiple times. Poetry is rarely read just once, and some styles of recitation include repeating of sections. Each additional presentation of some language offers us a chance to change other aspects of style such as tone of voice or visual style or emphasis pattern.

## Concrete vs abstract, examples vs assertions

Some style advice says "show, don't tell". The idea is to tell stories with concrete examples, describing particular experiences, events, and effects — rather than explicitly state claims and assertions.

We can state general points like "consider whether and how many examples to provide", or we can provide concrete examples and let audiences draw their own conclusions. We can also present both so that the examples (hopefully) support the explicit arguments. 

Though it might not fit most contexts, a powerful satirical effect can come from challenging audiences with boldly contradictory messages — examples that suggest one conclusion, paired with explicit assertions of an opposing point.

## Ambiguity vs clarity

Language can express precise clarity or vague generality. Ambiguous and hesitant wordings can obscure understanding. But maybe such ambiguity *could* sometimes be desireable. Some sorts of contexts might kinda call for imprecise wishy-washy impressions rather than conclusive, concrete certainty.

With appropriate language, we can *hint* at ideas without committing to them. One subtle option is using "-ish" as in "greenish" vs "green". We can avoid specifying with generic terms like stuff, things, folks…

Intentional use of language that has multiple interpretations possible…

specification, underspecification; generality (wider application, easier to infer one's particular ideas / experiences etc)

## Language play and creativity

Overall, we can approach language with humor, silliness, playfulness, and other creative attitudes. The Wikipedia article on [**figures of speech**](https://en.wikipedia.org/wiki/Figure_of_speech) lists a huge variety schemes and tropes. Exploring these can greatly expand our language style options.

An obscure but notable example is kennings, a common device in Old English and Old Norse poetry using compounds that illustrate metaphors or specific perspectives, like calling a body a "bone-house".

We can also explore and go beyond the boundaries of normal syntax. We can mix languages and styles in unexpected ways such as using one language's syntax with another's vocabulary or switching suddenly between formal and slang styles.

Deconstructing language can free us from linguistic focus. We can learn to take language less strictly, less seriously, and less literally.

## Wordings and their social impacts and sensitivities

As described earlier, language style includes both expression and interpretation. The people involved will approach things differently based on mindset, emotional state, habits, and past experiences and associations. Understanding these contexts can help us use language more appropriately.

### Politics, marketing, norms, and euphemisms

Political and marketing researchers use focus-groups and other research to learn how people react to different language. For example, talk of government "programs" gets more resistance than talking directly about the purpose ("welfare program" vs "assisting those in poverty"). To "not give" feels less harsh than to "deny". "Retirement security" feels more clear and meaningful than "social security". It feels different to talk about problems with "crime" vs the importance of "public safety".

The reasons vary why some language gets different reactions or more acceptance than other language. What matters here is the recognition that knowing the audience enables more effective use of language. We can consider language choices *successful* to the extent that they achieve the *intended* results.

Language around contentious topics tends to keep changing. Language originally intended for sensitivity can lose that association over time. For example, the term "handicapped" originally came from horse racing and giving extra challenge to the fastest horses (like a weight, extra distance, or late start) in order to give all competitors a more equal chance. Describing people as "handicapped" implied potential strength and resilience with extra challenge to overcome. It was a gracious and respectful term compared with older words like "crippled". But "handicapped" was not otherwise a familiar word, so the implications were not widely recognized. The more clear term "disabled" was then chosen despite its lack of grace or nuance. Some people adapted that to "differently-abled" which can feel a bit ambiguous and contrived and is not appreciated by everyone. How can we do better? One option is language of "impairment" or "impaired" (usually specified *in what*). But many people prefer *positive* or neutral language rather than negative. Labels like "deaf" and "blind" have no inherently negative style, but they have a discrete and categorical style rather than one of a trait on a continuum. One word that seems to avoid all pitfalls is "accessibility" — emphasizing the *positive* ability to access things. How can we describe people with accessibility challenges? Well, that works: "people with accessibility challenges". We can specify as needed, "people with accessibility challenges due to vision/hearing/mobility…". And we might recognize that the *challenges* disappear in contexts where the same people have no trouble with access. So is it the *technology* or the *building* or whatever that has accessibility challenges rather than the people? The focus can be on accessbility accommodations. So, maybe the verboseness is worthwhile. Maybe talking this way about the challenges that a context presents to some people is worth the extra words. And maybe taking similar conscious consideration for how we describe other traits is likewise worth the time and effort.

Unfortunately, language can get distorted as it gets used carelessly or even gets intentionally misused. Sensitive words that get used by everyone can end up losing their sensitivity. And carefully *euphemistic* language may eventually get called out and then get associated with pretense or deception. For example, people now talk of "greenwashing" when products get marketed as environmental/sustainable/healthy/green/natural (and so on) when such descriptions are dishonest or unremarkable (for example, labeling bread as "plant-based"). A related idea is "openwashing" where products are labeled "open" for no good reason (or for intentionally deceptive reasons). These trends make it harder to honestly describe things that are indeed environmentally healthy or are meaningfully open.

For effective language to work long-term, it needs to be in a context with sincerity and integrity. It can be appropriate to enforce some consistency in language, whether by only social pressure or even legally. Still, language continually adapts, so we will always need to adapt our conscious use of it.

### Sacredness and taboo

For various reasons, people come to extra sensitivity about some language. Some language violates social norms in exceptional ways, and using it at all can be transgressive. Other language is treated with particular reverence, and it violates the norms to use that language casually outside of the respected contexts. These norms change over time and vary between different cultural contexts. Whether it is healthy to respect or to violate such norms is yet again something to consider through attuning to each particular context.

### Using references and associations

How much do we talk about ideas with reference to particular people, cultures, religions, philosophies, sources, and other associations?

Labeling associations can help with memory and accessibility. We can maintain a more neutral point of view when we mark the sources of ideas — compare: "X is true" vs "source says X". Highlighting sources also helps to express specific appreciation and to ease further investigation of context and related ideas.

The main risk of references and associations is in bringing up prejudices which distract from direct understanding.

When bringing up references, we can consider separating concepts from meta discussion about sources. One way we can reduce prejudice is by using diverse sets of references (such as mentioning the corresponding versions of similar concepts from Buddhism, Stoicism, psychology, Christianity, etc. rather than only describing one).

### Appropriate language choices

When considering our language choices, we can consider research and advice from others, and we can also directly use the attune-resolve-review process. We can test out how *we* feel with different language, try it with others, get their reactions and feedback, and review and update. Some choices will work well in general, but some audiences and contexts will call for different language than others.

Conscious use of language incorporates all these various aspects along with the context. Does conscious language always mean *respectful* or *honest* language? Not necessarily. We can consciously use insults and deception — understanding that these are *constrictive*, related to defense, to fight-flight, and that *appropriate* use means only to engage in such styles when situations really call for it. Can you imagine a situation where insulting language helps keep someone safe? It may be easier to imagine all the ways it can be dangerous, so maybe it's a good rule to avoid it. But there may be exceptions, and we can only decide by consciously considering each particular situation. Like punching a pillow to let out anger without hurting anyone, we can more safely use some language privately. Even then, whether such methods of processing thoughts and feelings are healthy and appropriate depends on whether they lead to overall healthier outcomes rather than reinforce unhealthy habits.

### Conscious language example: Freddish

Fred Rogers was exceptionally conscious in his language style both in his life in general and in particular for his children's show *Mister Rogers' Neighborhood*. Some of his staff wrote a summary of the process they observed Fred taking in the particular context of the show:[^Freddish]

> 1. “State the idea you wish to express as clearly as possible, and in terms preschoolers can understand.” Example: _It is dangerous to play in the street._
> 2. “Rephrase in a positive manner,” as in _It is good to play where it is safe._
> 3. “Rephrase the idea, bearing in mind that preschoolers cannot yet make subtle distinctions and need to be redirected to authorities they trust.” As in, “Ask your parents where it is safe to play.”
> 4. “Rephrase your idea to eliminate all elements that could be considered prescriptive, directive, or instructive.” In the example, that’d mean getting rid of “ask”: _Your parents will tell you where it is safe to play._
> 5. “Rephrase any element that suggests certainty.” That’d be “will”: _Your parents can tell you where it is safe to play._
> 6. “Rephrase your idea to eliminate any element that may not apply to all children.” Not all children know their parents, so: _Your favorite grown-ups can tell you where it is safe to play._
> 7. “Add a simple motivational idea that gives preschoolers a reason to follow your advice.” Perhaps: _Your favorite grown-ups can tell you where it is safe to play. It is good to listen to them._
> 8. “Rephrase your new statement, repeating the first step.” “Good” represents a value judgment, so: _Your favorite grown-ups can tell you where it is safe to play. It is important to try to listen to them._
> 9. “Rephrase your idea a ﬁnal time, relating it to some phase of development a preschooler can understand.” Maybe: _Your favorite grown-ups can tell you where it is safe to play. It is important to try to listen to them, and listening is an important part of growing._

[^Freddish]: as referenced in the book [*The Good Neighbor: The Life and Work of Fred Rogers*](https://search.worldcat.org/title/The-good-neighbor-:-the-life-and-work-of-Fred-Rogers/oclc/1024140851) by Maxwell King
