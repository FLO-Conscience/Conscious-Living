# Styles of modeling

One common form of thought is abstract *models*. While we can consider all mental images to be models, the emphasis here is on logical and mathematical modeling.

A particular model might make connections such as the feedback loop between healthy sleep and emotional regulation. When we consider the *styles* of model, we are discussing the ways we generally build models. Modeling styles get into questions like how do we mark connections and how much do we use or emphasize ideas like feedback loops overall.

## Traits vs types

Categorizing by types means putting sets of things into distinct groups with labels. Traits are characteristics/features that anyone or anything might have or not have, or have more or less, or have one or another version. Types can be defined by particular traits, and we can describe types of traits.

Overall, thinking in traits leads to models with open fields with a wide variety of combinations and similarities and differences. Thinking in types tends to support models with more defined categories. Types are more like boxes while traits are more like tags.

## Continuous vs discrete

Alan Watts described thinking styles that prefer models to be either prickly or gooey — either with distinct, discrete objects and defined edges or with blurry spectra, everything fading together in a kind of *bleah* with no hard lines or distinctions. A prickly world model emphasizes *particles* while a gooey model emphasizes *waves*.

And Watts emphasized that even understanding these two styles relies on seeing the contrast between them. Seeing these styles with stark contrast is itself a discrete (prickly) way of thinking, while a continuous (gooey) thinking suggests that we can have intermediate models with prickly-goo or gooey-prickles — wavy particles or particular waves. A model can also have continuous features in some aspects and discrete features in others.

## Number of dimensions

No model can account for every factor of reality (doing so would make it be the entirety of reality itself, not a model). So, which dimensions do we include and which do we ignore? We can tend toward modeling more factors or fewer factors.

## Graph styles

Graphs are a common form of model, and there are many graph styles. We can think graphically in subtle and implicit ways without visualizing precise graphs. For example, people propose seeing history as cyclic or not. Whether fuzzy and implicit or careful and explicit, graphing styles affect how we consider connections and relations.

Graphical models can take many (possibly overlapping) forms:

- networks (the most common meaning of "graph" in mathematics)
    - trees
    - webs
- circles
- spirals
- curves (compare the common use of "graph" as in functions)

Some of the main traits a network model can have:

- directed or undirected
- connected or disconnected (are there parts of the graph that have no connection path to some other parts)
- cyclic or acyclic (can connections make loops)
- finite or infinite
- linear or nonlinear
- branching or not
- multigraphs (can there be more than one edge between two nodes)
    - loops (can an edge connect a node and itself)
- hyperedges (can networks connect more than two nodes at once)
- edge and/or node weighting
- static or dynamic

And curve models can follow or not follow common shapes (bell curves, J curves, and so on) with associated implications.

## Probability and statistics

Our attitudes toward probability and statistics influence the way we think about and develop models. For an explanation of different views, see <https://en.wikipedia.org/wiki/Probability_interpretations>
