# Presence and acceptance

In the teaching of mindful presence, a common principle is *acceptance*. Rather than avoiding or resisting, we are told to allow experience to be just as it is.

## The paradox of acceptance

Avoidance and resistance are themselves part of experience. Does accepting everything as is include accepting our feelings of non-acceptance?

It seems like a paradox. A wish for all our resistance and constriction to release is *itself* resistant and constrictive — a wish for the world to be different than it is.

Perhaps we can indeed experience complete acceptance, free of *any* constriction. However, we cannot get there by *constricting* around the idea of complete acceptance. Maybe we can only get there by not trying to get there…?

## Acceptance in layers

We can understand acceptance in layers. Acceptance in the context of noticing constriction can mean not getting further *constricted about the constriction*. Can we accept feeling just the level of constriction we feel? And if we do not, can we at least accept that we feel constricted about our constriction rather than add yet another layer? Can we avoid feeling constricted-about-constriction-about-constriction? Acceptance has to start *somewhere*. Maybe it starts with laughing about the absurdity of this whole premise.

We can relax our constriction layer by layer. We might start with the unhealthy idea that constriction is bad. Instead, we learn to appreciate constriction. We accept how essential constriction is for our safety and health. With that perspective, we feel less constriction-about-constriction. From there, we are more ready to process our primary constrictions.

A key idea to processing constrictions is to accept them in the moment here and now without thinking that they should necessarily strengthen or loosen. Everything changes, and we can accept that the constrictions will shift and adjust along with the changing context. A *healthy* approach includes listening to and responding appropriately to our constrictions. We don't need to release them for the sake of releasing them. At this stage, we accept constrictions, and we accept that we *don't accept* whatever we're feeling constricted about. We can trust that constrictions will release *when* we resolve whatever issues are causing them. And we can *appreciate* the constrictions showing up while the issues remain unresolved.

## Pure consciousness does not constrict

There is one other notable path toward acceptance. We can learn to bring attention to the open space of consciousness itself which has no qualities, for which we have no position from which to judge it, and within which everything in experience exists. Consciousness itself does not constrict and expand. It has no perspective, no reactions, no thoughts, and no sensations. Consciousness is the experience *within which* all these other events show up.

Perhaps acceptance is only (or primarily) the *absence* of resistance and rejection. In that sense, consciousness is fundamentally accepting of everything. There is no place for resistance — or really for energy of any sort — to be in *relation* to consciousness itself.

Somehow, we *can* direct attention to the fact of being conscious. But teaching how to do that is inherently challenging because consciousness itself cannot be described. We can't look at it from a distance or hear it or sense it in any other way. We can use metaphors to try to point to it. We can use techniques like in Zen to frustrate and befuddle all efforts to put it into language and concepts. We can engage in meditative practices. Some teachings seem to work more effectively than others at getting people to have this experience with awareness centering in pure consciousness. Still, it is an experience we can simply have or not with no plain way to directly communicate it to anyone else.

Notably, practicing this sort of pure conscious present awareness tends to bring other changes within experience. Constrictions *within* consciousness tend to relax more easily and show up less often. Mindfulness practice seems to bring increased experiences of acceptance, appreciation, love, and compassion. We can do such practices and benefit from the results with an open curiosity rather than a constrictive yearning for enlightenment (which seems to itself get in the way of progress).

For more about this sort of pure-conscious-awareness perspective, read more about [mindfulness](Mindfulness.md) and [non-dual awareness](Non-dual-awareness.md).

## Multiple ways to practice

Few of us will give up the rest of life's pursuits to focus on maximizing meditation time. In practice, we can do a mix of dedicated meditation practice, momentary repeated presence reminders throughout other activities, and consciously working on directed CL practices where we work on responding in healthy and appropriate ways to specific parts of our lives.

Meditative enlightenment is not necessary in order to work on any particular acceptance. We can also find acceptance through other thought processes and CL practices.
