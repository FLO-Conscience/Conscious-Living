# Observation vs Imagination

In describing conscious experience, we can mark a distinction between observation and imagination.

We observe thoughts and sensations. Some of the thoughts come in the form of mental images. Some of those mental images are memories of past observations. Some mental images are speculative adaptations, things we have never directly observed.Those we call imagination.

## Describing observations

Observation starts with the sensations we directly experience. We see, hear, touch, taste, smell, and so on.

When we mentally note or communicate about sensations, we rely on abstractions like words. Still, we can use language that points as directly as possible to the raw sensations. We can do that by describing the details of making observations themselves.

We can say, "I see [description of what we see]" or just "seeing [description of sights]". 

The words we choose to note sensations inevitably impose concepts and require some interpretation. That said, we can consciously hold such markers lightly and acknowledge that they are rough, imperfect designations that go with the full ineffable experience. We can do our best to intentionally choose language that avoids extra implications and needs minimal interpretation.

We can describe thoughts by marking them explicitly as in, "I notice this thought…" or "a thought:" In that way, we describe observation of the thoughts without asserting that any claims in the thoughts themselves are observations.

## Imagination

Some people associate the word "imagination" mainly with creative fantasizing. We imagine fictional things that have never existed like dragons or interstellar travel or simply fictional people having conversations in realistic scenarios. 

In CL, we understand "imagination" to cover even likely real-world things that we do not directly observe. A family member goes shopping and comes back with groceries, and we can *imagine* them going to a specific store, putting groceries into their bag, and so on. Our imagination might nearly match direct observation from the past, but we still have only imagined and not observed what happened this time.

Imagination is usually the closest we can get to seeing someone else's perspectives. We observe someone's face, and we directly experience our own reactions (which may include empathetic feelings and even the matching of others' expressions). We do not actually observe *their* feelings. We only *imagine* what their subjective experience might be. We *observe* frowns and cries; we *imagine* someone's sadness.

## Describing memories

Many mental images are memories of observations. Even though memories *are* a form of thought and aren't raw sensory observations, calling them "imagination" would be confusing.

When it's appropriate to be extra clear and distinguish immediate observations from memories, we can say "memory of observing…". Often, that distinction works well enough being only implied. We can say "I remember seeing them cry" (clearly a memory of observation), but "I remember they were sad" involves more interpretation. So, we might distinguish observation and imagination by saying, "I remember them crying, and I imagined how sad they must be feeling." 

## Practical rather than dogmatic distinctions

We could not think and communicate effectively if we constantly marked everything as observation or imagination. In practice, we can mark the distinctions where it seems helpful. We can consider whether it might avoid misunderstandings or help with maintaining humble curiosity and avoiding overconfidence. We can try noting the distinctions and review whether it was worthwhile.
