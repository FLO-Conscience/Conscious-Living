# Non-dual awareness

In common everyday experience, we project mental-models, distinctions, and other concepts onto our thoughts and sensations. Many of these projections come as dichotomies like content-vs-context and self-vs-other.

We can learn to relax all this conceptual projecting. At the extreme end of such a practice, thoughts and concepts may fully cease, leaving only raw sensations. For most of us, most of the time, thoughts will still arise, but we can learn to experience them mindfully *as thoughts* while *also* being aware of raw sensations unbounded by concepts.

The sort of "non-dual" awareness most emphasized in meditation teachings is the dropping of the self-other dichotomy *while* remaining mindful. In that state, we can drop all the normal concerns that lead to caring about some parts of experience over others. This view can release constrictions and compulsions and bring a sense of freedom and acceptance.

Unlike nihilism (the belief that nothing has value or meaning), non-dual awareness retains and includes feelings of curiosity, play, love, and compassion.

## Internal vs external and self vs other

We often see things within the context of an internal vs external dichotomy. We integrate some sensory information into an image of a self and typically include thoughts as well (we only experience *our* thoughts, not anyone *else's* thoughts). We treat other sensory information as going with external other things that are not ourselves.

We generally treat things very differently when we identify with some features as internal and self rather than as external and other. Bringing attention to how we impose these views can greatly affect how we deal with things. Once mindful of these patterns, we can more readily change them and/or relax them.

## Releasing the self-other dichotomy

Experiencing non-dual awareness may follow a simple sequence.

Once we have adequate practice with concentration and mindfulness, we can mindfully notice individual thoughts and sensations without identifying with them. No thought or sensation is "me", is *who I am*.

Widening awareness enough, we can mindfully experience everything as mere contents within consciousness. The only context is mindful awareness.

### Imagining an observer-self

Mindful awareness itself can still feel like *observing* the contents of consciousness. We can then naturally imagine some agency, a sense of an *observer* doing the mindful observing. We imagine this sort of *observer-self* existing in relationship to the contents of consciousness. 

We may imagine something like watching a movie or playing an immersive video-game while retaining some awareness of being the person watching or playing.

Possibly because of the way our senses of vision and hearing work, we often have a body-image in which awareness is centered in the head. So, we imagine the observer-self being somewhere behind the eyes, taking in sensory inputs and willfully directing attention.

### Releasing the observer-self

To release this observer-self image, we can attempt a content-context flip, a figure-ground reversal. This works best after practicing such a flip in other contexts. We can look at a scene, notice our focus, and flip attention to what we initially treated as background. What happens when we try this while opening awareness to all thoughts and sensations, leaving only mindfulness itself as the context?

Of course, observation cannot observe itself. In the instant of trying an impossible content-context flip, the premise of context/content distinction may fall away.

Similar methods of inquiry include the instruction to "look for the looker". Or we might also ask "who" is doing the seeing or the hearing. Then, we notice that seeing and hearing *just happen*, we don't *do* anything to *make* them happen. All attempts to find some distinct observer or agency or self will always immediately fail. We only experience the ever-shifting process of thoughts and sensations.

Non-dual awareness remains mindful yet is also free of any sense of context or observer-self. In describing this view, we cannot say that we experience only context or only content because the distinction only exists in the duality, in the contrast. We may say there is *no self* or that *we are everything*, but really the sense-of-self disappears as it only exists in the dichotomy of self-vs-other.

## Features of non-dual awareness

Of course, non-dual awareness does not erase other understandings of "self" or differences between people or differences between subjective conscious experience and presumptive reality outside of consciousness. We can still have abstract concepts of being a person with all our particular characteristics, being where we are, and doing what we're doing. What we drop is the dividing of *conscious* experience into features of self or other.

Also note that non-dual experience retains dynamic complexity. We still having different sensations like sight and sound, light and dark, and everything else. It's not like things blur together and lose form.

We can still predict that pinching our arm will bring a sensation of pain while pinching someone else's arm will not. We still experience differences between voluntary actions and involuntary muscle spasms (this has something to do with neurologically predicting voluntary actions).

## Insights from non-dual awareness

Various benefits and insights come from non-dual experience. It tends to bring equanimity, relaxation, and understanding.

Non-dual awareness allows us to treat everything with more equal status. We need not treat painful experiences as enemies to hate and defeat. We need not wish for anyone or anything to win over others. We need not see things as problems to solve or think that things *should* be any different than they are. Or rather, any thoughts like that are themselves not something we identify with and care about over any other thoughts and sensations. All constrictive and expansive energies, all stories, all relationships — they are all showing up together in the dance that is everything in consciousness.

Non-dual awareness brings a fundamental acceptance because we no longer imagine a set of conflicting scenes like the current situation versus an imagined improvement or a feared worsening. Instead, we see all of it, including these images of improvements and regressions as thoughts, all showing up in one scene. There is no fundamental "self" who *wants* things to go one way or another, there are only energies and images.
