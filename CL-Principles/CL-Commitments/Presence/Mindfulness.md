# Mindfulness

Mindfulness is a sort of *meta-awareness* — awareness of being aware. With mindfulness, we consciously recognize thoughts and sensations *as* thoughts and sensations, as experiences within consciousness.

Mindfulness has many effects. When mindful, we feel less completely absorbed in experiences. We can notice everything with a sort of detached consideration. Mindfulness tends to bring more relaxation, less compulsion, and other changes.

## Practicing mindfulness

Various prompts and cues can induce mindfulness. We can practice mindfulness to make it a more frequent and stable feature of our lives.

There are many styles of mindfulness training. Mindful presence can work with a narrow focus on breathing or other precise parts of consciousness, or it can involve a more wide open view.

To experience sensations *as* sensations directly, it can help to drop normally imposed concepts and images. Normally, we experience [multimodal integration](https://en.wikipedia.org/wiki/Multisensory_integration), the development of mental images through combining many different senses and thoughts. To more mindfully experience sensations, we can focus strictly on a specific sense like vision or hearing, aiming to experience it prior to integration with other senses and prior to labels and concepts.

Interestingly, if we ever check whether we are in a mindful context, the answer seemingly has to be "yes". Even if we were *not* mindful to start, inquiring about the state of mindfulness will cause a shift to mindfulness. We can never be mindfully aware of non-mindful experience. We can only review in memory that we do not recall mindfulness in a past moment.

## Mindfulness as a contextual experience

Attention commonly focuses on some things and leaves other things as background. This [content vs context](/CL-Principles/Context-vs-Content/Context-vs-Content.md) differentiation is a basic feature in most of conscious experience.

Mindfulness involves a sort of expanded awareness, recognizing all experience as contents within consciousness. However, mindfulness still leaves room in working memory for meta-awareness, for mindfulness itself. That is in contrast with states like *flow* where we focus so intently that no room is left for awareness of context — not even for mindful awareness. 

## Limits of meta-awareness

Mindfulness can include noticing the experience of mindfulness — awareness of our meta-awareness (meta-meta-awareness?). These layers seem conceptually infinite, but we cannot consciously experience infinite layers. Though we can be mindful, we cannot make mindfulness itself the direct focus of attention. We cannot view consciousness itself from some outside perspective.
