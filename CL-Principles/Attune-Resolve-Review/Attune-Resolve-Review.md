# Attune-Resolve-Review

The core process of CL is a cycle of attuning, resolving, and reviewing (ARR for short).

## Attune

We *attune* by bringing mindful attention to thoughts and sensations. Beyond mere perception, attuning implies sensitivity, interest, and concern. Attuning is an *active* process of bringing conscious attention into alignment with present experience. We can attune *to* particular aspects of experience or to a more holistic, open wide-angle view. 

Terms related to attuning: notice, observe, focus, attend

One form of attuning is pre-cognitive mindful presence: just taking everything in without judgment — perhaps without even labeling or distinguishing particular features of experience.

We can also attune in a more directed manner. While exploring what we notice, we can evaluate contextual features such as feelings of constriction or expansion. We might label patterns of thoughts and sensations as feelings like fear, anger, sleepiness, curiosity, gratitude, and so on. We can note specific concerns that arise. 

Some words related to this cognitive style of attuning: assess, appraise, discern, describe, evaluate, inspect

## Resolve

Resolution means relieving concerns that we notice when attuning. In every situation, we have choices about how to respond. Resolutions can take many steps, and options for next actions can range widely. We might do some immediate bold intervention, or we might take time for careful planning. Often, initial resolution comes from simply doing more attuning.

## Review

Review involves attuning again while considering our past responses. We attune to our memories, viewing things with hindsight. We can recall our prior process of attuning and resolving and then attune to the thoughts and feelings that arise during our recollection. Then, we can consider what issues remain to still resolve, what insights we can learn, and what adjustments to make in our process going forward.

A review may consider questions like: How well did we attune? How appropriate and effective was our response? Did we overreact or underreact? What could we do to improve our responses in similar future situations? What do we need to attune to and resolve next?

## Overlapping and fuzzy stages

We need not think of ARR stages as some strict, distinct sequence. In any moment, we can recognize aspects of all the stages occuring at once. And we might find it helpful to mark additional steps and details within each stage.

All the stages involve the others. As we attune, we *resolve* a lack of initial focus by choosing among methods of attuning. As we work to resolve a concern, we attune to our thoughts about resolution options and how we feel about each one. All the while, we draw on memories from past practice and study.

As we ingrain the ARR process in our habits, we can learn to use it intuitively without explicit linguistic thinking or other marking of distinct steps. When we know the whole process, we can spark it with just the prompt to attune, trusting that we will move through the other stages from there.

## ARR application: context before content

The CL process applies at multiple levels and works continuously as an ongoing pattern of our lives. It works with topics at every scale from everyday tasks to concerns about the state of society and the planet.

Usually, CL does not call for jumping straight into resolving the problems in the world. We first consider our [context](/CL-Principles/Context-vs-Content/Context-vs-Content.md), starting with constriction and expansion.

After bringing initial attention to a topic to process, the full ARR sequence might look like this:

1. Attune to our feelings of [constriction or expansion](/CL-Principles/Context-vs-Content/Constriction-and-Expansion/Constriction-and-Expansion.md)
2. Check for *constriction about constriction…*
    - Do we accept the constrictive and/or expansive energy we are feeling?
3. Process layers of constriction (and/or expansion) one at a time
    - Apply ARR process and tools, considering various aspects of the context, learning and adapting until we have broader perspectives and a relaxed mindset
4. Process *content* that we feel ready to address

The level of depth and details can vary enormously. Minor everyday things will usually need barely any attention. Major issues may take many hours spread out over days, weeks, months… We *can* infinitely explore anything. No matter the situation, everything happens in tiny steps, moment to moment.

<details>
<summary>Example ARR process [click to expand]</summary>

Note that the following simple model example *could* fly by in a smooth, flowing process in a fraction of the time that it takes to read this description of it!

Issue: Thinking about not yet receiving a reply to a message. Attuning reveals constriction. Noticing *additional* constriction upon noting the initial constriction (as in thinking "shouldn't feel constricted about this"). Considering resolution tools, recalling the thought that constriction means caring ("constriction means *caring* about this communication"). Noticing some relaxing of the constriction. Noticing constrictive thoughts about the message ("if *they* cared too, they would have replied by now"). Noticing growing expansive energy when thinking about investigating the constrictive thoughts ("exploring these thoughts will be fun and interesting, there are other perspectives to see"). Exploring the contexts of thoughts about the message and other ways to think and feel ("*imagining* that they don't care, but the only *observation* is not seeing a reply"). Noticing more overall relaxation. Noticing expansive energy, feeling motivated to explore healthy responses to the situation ("could shift focus to something else for now to give them more time, could send a reminder message, could try a different way to reach out, could ask a mutual friend for perspective or help…"). Choosing a response that feels okay even after checking for constrictions ("will send a brief reminder"). Reviewing, checking for feelings and thoughts about how to improve CL practice and what else to do about the situation ("we have no clear agreement about timely responses to messages; can make a note to discuss that in a future conversation at a time and in a style that feels right; can plan for how to make that go well…").

CL process will go smoother and faster as we improve our practice. In this case, after achieving clear agreements about message replies and having clear options for how to respond to any delays, we might avoid future worries about others not caring. Or maybe it turns out that others indeed do not care or worse, and *then* we have a whole bigger topic to process. Whatever the case may be, CL practice builds CL skills that serve us in whatever circumstances we face.
</details> 

## Apply to ourselves

**CL applies to ourselves.** Our context can involve various people and circumstances, but the CL process does not tell others what to do or determine what others are experiencing. Any thoughts about others and what they should do are part of our own experience.

Of course, resolutions *can* involve telling others what we think they could or should do — including coaching others through doing the CL process. However, it's often healthier to lead by example and to focus on listening and questioning. Still, appropriate resolutions depend on the contexts and particular people. Whatever the case, we can only choose how *we* respond to any situation.

## Tools to use in the process

**The ARR process can consider context before content**. When attuning, we can investigate the contexts including the sorts of stories and style in our thoughts. When resolving, we might respond by adjusting our stories and style and so on.

**The ARR process inherently uses the CL Commitments** (presence, responsibility, curiosity, integrity, love and compassion, and vitality). Effective attuning requires presence and curiosity. Choosing to do the CL process is itself taking responsibility. Skillful resolutions will align with the other commitments. Things will not go as well if the decisions are out of integrity, lack compassion, or go against our overall well-being.
