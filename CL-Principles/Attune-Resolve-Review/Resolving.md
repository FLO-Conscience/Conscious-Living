# Resolving

Resolution is the second stage of the attune-resolve-review process.

## Resolve enough to review

Whatever we find when attuning, resolving means deciding what to do next. **Effective resolutions release our energy enough to allow us to move on to reviewing** (and then from there to loop back and start the process again with attuning to whatever comes next).

## First stage of resolution: acceptance

Often, we feel unwilling to allow a situation to be as it is. We constrict, and we want to deny our experience or insist that it change.

We cannot effectively resolve anything else about a situation until we resolve our resistance to being with the situation. We start by accepting the constriction *itself*. Acceptance releases constriction-about-constriction. From there, we are free to process the remaining concerns.

For a more thorough process, we can use the [4 A's](/CL-Practice/4As.md): Acknowledge, Allow, Accept, Appreciate

## Resolving constrictive energy

Resolving constrictive energy involves making sense of perceived threats.  When we judge a threat as real, we must take steps to either protect what is at risk or to release our attachment to it. Other times, we realize that there is no threat after all, and having that realization relaxes the constriction. We might also use various conscious relaxation techniques.

## Resolving expansive energy

Resolving expansive energy involves either acting on our inspirations or reconsidering them. Acting with expansive motivations can mean engaging with play and love and curiosity. Reconsidering our expansive energy means thinking critically and getting in touch with constrictive perspectives and insights in order to see problems we may have missed.

Sometimes, we feel motivated not to engage with a specific topic but to explore and open ourselves to new topics. We might seek out sources of inspiration or sources of challenge and constriction. Either direction can offer opportunity for learning and progress.

## Emergencies

Strong feelings of either constriction or expansion often come with a feeling of compulsive urgency. In those cases, we must assess whether the situation is a true emergency. Sometimes, the best resolution involves taking urgent action without delay. We may need to escape immediate danger, treat an urgent medical condition, kick the ball into the goal, or tell someone we love them in what might be the last moment we see them. We cannot always afford to do an explicit, deliberate CL process. Ideally, with enough CL practice, we can develop our intuitions enough to take appropriate emergency actions without even thinking.

## Compulsion

Often, people feel compulsive urgency in situations that are not true emergencies. For many circumstances, a good default resolution is to simply pause for a few slow, conscious breaths. In the next round of the process, we might decide to go for a short walk, drink some water, do a longer meditation, write out our thoughts, or explore other ideas from the CL commitments. Often, resolution comes from finding acceptance and appreciation for the insights that came from our attuning. The parts of us with constrictive or expansive energy can begin to relax when they feel truly heard.

Generally, CL calls for more deliberate, less-compulsive engagement. We often do well to take time to process and gain perspective, engaging no more than necessary. Skipping the CL process will tend to result in continued problems. The healthier and better-calibrated our process, the more we will see progress.

## Looking forward, anticipating review

When considering resolution options, we can aim for successful outcomes in alignment with commitments and with minimal side-effects.

Before acting, we can imagine how things will work out. We can play out different options in our minds as a sort of simulated trial-and-error process. We can also ask ourselves how we predict the reviewing will go (one term for this is *prospective retrospection* — meaning we look ahead toward when we will be looking back). With practice, we can learn to sense when we are *about* to act inappropriately; **we can ask, "am I gonna regret this?"**

## Gathering perspectives

To gain perspective before further action, a good process might look like this:

- First, write down the ideas that show up in the current state, and plan to review them later.
- Then, find a minimum appropriate resolution to relax immediate energy.
- Later, review the topic again from a different mindset and compare with the prior ideas.
- If still sensing a pattern that needs resolution, find options that accommodate the insights from the multiple perspectives and that fit the CL commitments.

## Addressing symptoms versus causes

In general, appropriate resolutions focus on underlying issues rather than on treating symptoms. However, we might focus on moderating the symptoms just enough to relax and then make a considered plan for the overall problem. Addressing symptoms directly can be a fallback option when resolving the underlying problem seems infeasible. When we don't know how to relax the symptoms directly, we can focus on building tolerance and mindful equanimity. We can also appreciate how experiencing pain can bring us perspective that helps us to more gratefully notice our pain-free times and to be more compassionate with others when they experience pain.

### Calibrating sensitivity

Note that signals are not perfect. We might experience pain or fear without any real injury or danger. On the expansive side, we can imagine amazing opportunities that are not real. In these cases, resolution involves recalibrating. Depending on the situation, we might need to adjust our thought habits or work on desensitizing or on building sensitivity.

## Subtlety

When we feel only mild constriction or expansion, we can go with the flow. We might still choose to do longer attuning, investigation, meditation, and processing anyway. Or we might just go with with whatever decisions show up, preferring to keep moving with whatever we're doing rather than overanalyze our options. When we have worked to calibrate our reactions well, then we can better trust our intuitions going forward.

## Allowing mistakes, avoiding perfectionism

In the process of learning and calibrating, we must be willing to make mistakes. All we can do in each moment is aim for our best, and then we get to experience the results. We can review how it went and use the insights to inform our future choices.

## Resolution tools and guides

See the separate collected [list of resolution tools]() for various practices and guides. Each tool has different emphasis and language. We can try the various ones for perspective and see which we tend to prefer overall or for different circumstances.
