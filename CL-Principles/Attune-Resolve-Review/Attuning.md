# Attuning

Attuning is the first stage of the CL attune-resolve-review process.

To attune, we bring conscious attention to immediate thoughts and sensations.

The focus of attuning can be open toward whatever shows up, or we might use prompts such as a list of topics in our lives or the list of CL commitments.

Attuning goes beyond passively experiencing thoughts and sensations coming and going. We can *engage* intentionally, treating thoughts and sensations as *signals*, like symptoms, that inform us about issues that need our attention. This framing makes it easier to appreciate whatever we notice rather than resist it. When pain shows up, we need not push away the pain itself. The experience may not be pleasant, but pain can notify us about an underlying injury or illness that needs attention.

As we investigate and contemplate our experiences, we can consider what messages and insights each situation offers.

## Attuning methods and questions

When attuning, various tools and tips can help. How much attuning we can do before moving on to resolution depends on the degree of urgency we feel in a situation.

### Attuning to constriction and expansion

We can ask about constriction and expansion overall:

- How am I feeling? (or where am I [in terms of constriction-expansion]?)
    - answers may include "very constricted" or "mildly constricted" or "mildly expanded, overall free and relaxed", "distressed with a high-energy mix pulling in both constrictive and expansive directions", etc.
- How much constriction do I feel?
    - perhaps answer with a score 1-10 or in terms like "slight" vs "strong"
- Where do I feel constriction? Where do I feel expansion?

We need not always use explicit wording. We might sense the state of things without getting stuck on precise labels.

Keep in mind that different parts of us can feel constriction and expansion at the same time and even around the same topics. For example, we can feel expansive inspiration to do some creative work while feeling constrictive attachment to the idea of achieving the work or feeling fears about whether the work will achieve what we hope it will.

### Assessing in terms of the commitments

We can inquire how our experience fits each of the commitments:

- How much am I experiencing mindful presence rather than distraction?
- How much am I taking responsibility rather than focusing on blaming?
- How much am I feeling open and curious rather than righteous?
- How aligned am I? Am I acting with integrity? Do I feel conflicted or hypocritical?
- How much are my thoughts and actions driven by love and compassion? Am I feeling any contempt or resentment?
- How well do I feel? More healthy or more ill?

Each commitment contains many parts and details that we can explore in depth. For example, we can ask questions like:

- What thoughts and sensations are here?
- What am I feeling called to do with this?
- What questions are worth exploring around this?
- What agreements are involved in this? What agreements do I want to make?
- How can I best feel and express love and compassion around this?
- What can I do to be as healthy as I can around this topic?

For any step in the process, any question, any inquiry, **we can gauge constriction and expansion via *willingness questions*** such as:

- Am I willing to use the CL process?
- Am I willing to pause and take some slow breaths?
- Am I willing to investigate this?
- Am I willing to work on resolving this?
- Am I willing to take responsibility, to get curious, to allow feelings, to appreciate this learning opportunity? (etc)

Willingness questions are inherently expansive. They can feel like friendly invitations instead of assertive mandates or commands. As long as we feel truly free to find and express any answer, the asking itself can help us to open up and actually find willingness. Of course, a constrictive imperative style might work well in some cases too. We can try different approaches and see how we react.

When we find unwillingness or non-acceptance, we can apply the CL process to those feelings. We can attune to the resistance itself and then choose an appropriate resolution. When we skip ahead and try to resolve constrictions that we do not even accept having, we will be less likely to get clear insights, less likely to find appropriate, effective resolutions.

When we notice a stronger "yes" to willingness or acceptance questions, we can then attune to and process those *expansive* feelings. What are they telling us? As with constriction, we can consider broader perspectives. If we just act on expansive energy, we might miss risks and dangers. We can ask whether there are concerns that might be appropriate to feel constricted about. When we have seen things from multiple angles, we can be more confident that we have a clear view and can make healthy and appropriate decisions.
