# Review

Reviewing is the third stage of the attune-resolve-review process.

## Perspectives from hindsight

Review reliably brings us different perspectives than the ones we get from our initial experience of events.

## Review as further attuning and resolving

When we review, we're still attuning and resolving in the present moment — except we do it with our focus on our memory of the prior situation. We recall the experience, and then we attune to the thoughts and sensations which arise with the memory. We finish (i.e. resolve) the review by updating our thoughts and attitude around the topic and making appropriate decisions that the review prompts.

For example, if we review that we took some action without conscious presence, then we might decide to prioritize reinforcing our mindfulness practice. Or maybe we review that we missed some perspective when deciding what to do, so we plan to practice more intentionally checking different perspectives. When we review a success, we might ask what worked well that was not already habitual and consider how to reinforce that as a stronger habit.

## Regret, remorse, and appreciation

Sometimes, review brings *regret*. With processing and compassion, constrictive regret can change into more expansive *remorse*. Regret and remorse can motivate our learning process and our decisions about what to do next. We can also feel appreciation for the insights the experience brought.

## Calibrating and refining

We can continually refine our reactions and our conscious awareness of them. We can test and tweak the various CL practices to learn which work best for each of us, for particular sorts of situations, for different levels of constriction, and so on. We will likely find some practices easier and others more challenging.

Tuning our reactions is like tuning pitches on a musical instrument. As long as we are in the general vicinity of the pitch we're aiming for, we can hear when we go too flat and too sharp, and that helps us find the middle where we are really in tune. When we explore the range of possible reactions and responses, we can allow ourselves to go a bit too far in any direction. If an adjustment leads to more problems, we need to go the other way. If things improve, we can continue in that direction until it feels like we've gone too far again. Through this process, we can find a balanced middle path.

Also like tuning a pitch, if we go *really* far out, we can get into a different place where we are no longer tuning toward the initial topic but instead are in the vicinity of some other (possibly related) topic. If we realize this, we can then continue consciously on the new topic or return to the original one. Tuning well requires both understanding the fine-tuning details and the overall big-picture context.

## CL skill-building

The more CL skill we gain, the more we can notice subtle signals and deal with them before they get stronger. As we adjust our lives, we can avoid repeating unhealthy patterns. We might experience times of more overall expansion with only subtle constrictions that we resolve quickly. Life-long CL practice can prepare us for the greater challenges we will surely face at some points.

As we gain skill at taking appropriate actions, we might simply live with less regret. Or we might try ever greater challenges, engaging with riskier and more difficult situations as we feel more ready to manage them. As long as we are alive, conscious living has no end point where we have completed the practice.
 
