# Healthy and Appropriate

In CL, we use the terms "healthy" and "appropriate" to mean effective, optimal, and aligned with the CL commitments.

Overall, judging what is healthy and appropriate is a subjective experience. In teaching CL, we generally avoid asserting universally whether any particular idea, feeling, or action is healthy or appropriate. We can each evaluate for ourselves what is appropriate or inappropriate in terms of our values, goals, understanding of the situation, and our capacity.

The view we can share in CL is that instead of accepting things without question, we consciously consider whether something is healthy and appropriate. So, we do assert that *asking whether something is healthy and appropriate is itself healthy and appropriate* — as long as we ask in healthy and appropriate styles at healthy and appropriate times…

<details>
  <summary>Maybe *prope* for short</summary>
*As "appropriate" is a somewhat long and cumbersome term, we could shorten it (at least in casual use) to **"prope"** (maybe written with an apostrophe: 'prope). The more that catches on, the more we might use it in various contexts.*

*A related updated acronym: **ASA'P** — As Soon As 'Prope (itself also a more appropriate sentiment than as-soon-as-possible)*

*Unfortunately, shortening "inappropriate" doesn't sound good as "inprope", so better options for that are "unprope" or "not prope" ("imprope" and "ain't prope" do not work as well because they sound too much like "improper" which has a different connotation).*
  </details> 

Whether our responses, reactions, and resolutions are healthy and appropriate depends on the circumstances. Extreme situations call for extreme responses, and moderate situations call for modest responses.

### General rules

General rules and guidelines can work much of the time as long as we adapt as appropriate. Put another way, even when rules are overall appropriate, it can sometimes be appropriate to break them. Of course, it can also be appropriate to adjust rules rather than break them, but it's probably never appropriate to try to make a perfect set of rules that will never need breaking.

## Inappropriate overreaction or underreaction

In the height of a battle in a war, it would be inappropriate to spend an hour planning next week's schedule. It would normally be inappropriate to leave a relationship over a single broken agreement about cleaning the dishes or to commit to a career without any consideration.

While a loved one is in hospice taking their final breaths, it would usually be inappropriate to go on a vacation to clear our minds before writing a careful letter to them — unless we feel too constricted to be present with them in a healthy mindset and need all that distance in space and time in order to find a loving perspective from which to engage.

### Calibration

Because we can always learn from any experience, we can't say strictly that overreaction or underreaction are themselves bad or unhelpful. We can, however, say that overreaction or underreaction are different from appropriate action. When we do overreact or underreact, the appropriate response is to review and recalibrate.

Note that calibration applies at multiple levels. Thoughts and feelings may be more or less healthy and appropriate for the circumstances, *and* our actions and expressions may be more or less appropriate for the thoughts and feelings. For example, we can assess that fear is appropriate for a situation but that hiding is an inappropriate way to deal with the particular danger.

Calibration works differently in different contexts. We may follow CL commitments excellently when relaxed but lose it when stressed. Energies of constriction and expansion continually shift, and we may notice well-calibrated responses to some states and less-calibrated responses in other states. We can work to calibrate our habits and prepare for how to do our best even in challenging circumstances.

## Appropriateness brings resolution

If some action resolves our tension, even just partially, that can indicate appropriate action. If stronger tension arises afterward, that might indicate inappropriate action. Our intuitive sense of inappropriateness may show up as a feeling of constriction when reviewing a decision.

## CL commitments guide appropriateness

We can check appropriateness according to the CL commitments. We can ask whether we make responsible choices with conscious presence, with curiosity, with integrity, with compassion, and aligned with healthy living. If we feel constriction around any of these, the constriction itself might be appropriate, and our next appropriate choice might be to process and understand the constriction.

## Appropriate constriction

Constrictive motivations are defensive, so they are appropriate when we need to defend something. In dangerous situations, staying safe may require violence or dishonesty that would be otherwise inappropriate. Of course, danger never justifies going beyond what is necessary for protection. General rules against violence and dishonesty are still important and healthy overall.

## Appropriate expansion

Expansive motivations are healthy when we feel safe and trusting enough to allow vulnerability, take risks, explore, and open up.

Appropriate levels of sharing take into consideration the whole context. Sharing ideas and advice with others works best when welcomed. In many cases, others are not open to our input. Creativity requires dedicated time, and so does staying healthy by taking breaks and attending to other needs.

## Related terms

Some terms related to appropriateness: healthy, helpful, skillful, calibrated, apt, optimal

Some terms related to inappropriateness: unhealthy, unhelpful, unskillful, overreaction, underreaction

Some other terms that can work but tend to have troublesome judgmental implications (many people feel constriction around these): proper/improper, just/unjust, right/wrong, correct/incorrect, good/bad, wholesome/unwholesome, smart/stupid, should/shouldn't
