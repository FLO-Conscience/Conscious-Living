# Style Guide for the FLO-CL Project 


## File structure

The documents here are organized in a **tree structure** for practical reasons and ease as an accessible and memorable mental-model. However, we welcome the contribution of alternative models/maps that organize links to the files in other ways.

## Marking and formatting

We welcome all sorts of helpful formatting and visual references that can make documents more skimmable and engaging.

*TASK:* Develop norms for headers, side-boxes, illustrations, other visual and semantic markings, tables, lists, etc.

*TASK:* consider warning symbols like ⚠️ for highlighting risks (recognized pitfalls of CL, dangers of misuse, times to get professional support, common misunderstandings); and explain the idea somewhere

## Citations, references, and sources

In FLO-CL, **we avoid claims that are not readily personally testable**. So, we need no appeals to authority. We have minimal citations in terms of asserting the validity of anything.

In rare cases where we want to include mention of scientific studies or anecdotes or specific mention of notable sources, we do then include appropriate citations as footnotes.

We do express *appreciation* for inspiring sources but usually keep that separate that from the main content. We may put such acknowledgements in Git commit messages and in the [CHANGELOG](/CL-history/CHANGELOG.md). We want to balance the goal of expressing appreciation with the goal of keeping ideas neutral enough and not overly associated with particular authors or traditions.

## Avoid commercial references

Promoting any commercial products or services would present a conflict of interest for the FLO-CL project. So, we avoid references to proprietary/branded/trademarked concepts and avoid mention of specific products or services. We accept generic references such as "yoga classes" or, as appropriate, even more generic as in "exercise classes".

## Language styles preferred within this repo

Overall, the language style here is likely to follow incidental patterns of the authors' habits, but we can consciously explore the wide range of styles described in the comprehensive [language-style](/CL-Principles/Context-vs-Content/Style/Language.md) article. May the style choices here vary appropriately and feel expressive and playful while communicating effectively and efficiently.

### "We" and "we can"

The writing tends to use first-person plural "we" to mean both authors and readers or more broadly "we people" (as in everyone in the world). Note that "we" especially works in FLO where we reduce the distinction between authors and readers.

In describing possibilities or pointing out features of CL, we often use "we can" as an inclusive style of coulding (rather than woulding and shoulding…) Example: "we can release constriction more easily when we appreciate it and respond to what the sensation is telling us".

### Vocab notes

**above/below**: avoid these when referencing other points in the CL docs, prefer references that will still work if things get moved around

**allow(s)**: avoid this when meaning "enables" or "supports", use "allow" only to mean permission

*TASK:* decide whether to specify how we use may/might: generally "might" for probability, "may" for a mix of possibility and permission?

*TASK*: do we have a preference among "like", "such as", "for example"? probably *avoid* "e.g."


