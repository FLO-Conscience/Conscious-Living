This document lists the principal authors and sources of the FLO Conscious Living resources originally published at https://codeberg.org/FLO-Conscience/Conscious-Living

These are not complete lists of every contributor or influence. All the sources were themselves built on other sources. And we have not prioritized keeping thorough and complete citations.

Citations are most valuable when an assertion cannot be readily verified by a reader themselves. Unlike some other topics, the CL ideas require no deference to expertise, authority, or historic evidence. Each of us can readily and safely test the ideas directly in our own lives.

We do care to express specific appreciations and to help readers understand something about how these ideas came together. We also extend profound gratitude to every writer, speaker, researcher, thinker, friend, relation, challenger, and more — everyone who played any part in shaping the world that enabled all this wisdom to come together.

## Principal authors

Those who have done substantial direct work on development and curation of these these CL resources:

- Aaron Wolf, aaron@wolftune.com
- Praveen Venkataramana, maxiwizz@gmail.com

## Principal direct sources

For the starting framework of this FLO-CL project, we adapted the concepts of "Conscious Leadership" as described by the [**Conscious Leadership Group**](https://conscious.is) (CLG) led by Jim Dethmer and Diana Chapman. CLG curated and synthesized their ideas from numerous sources into an accessible, practical, holistic structure with precise language. Several of their "commitments" are specific practices from the Hendricks Institute and The Foundation for Conscious Living, Byron Katie, and the Sedona Method. Some concepts draw on older traditions such as Buddhism and Stoicism, among many others. FLO-CL took the CLG concepts and put them into a FLO context, improved the structure, and then adjusted and expanded the concepts further both through bringing in additional influences and through iterative review and conversation. For thorough details on the intial transformation, see our full article about [how we adjusted the ideas from CLG](CLG-to-CL.md).

## Other notable authors, reviewers, and sources

