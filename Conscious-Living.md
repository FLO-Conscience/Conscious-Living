# Conscious Living

Conscious Living (CL) means life with mindfulness and intention. CL contrasts with a reactive, impulsive sort of life that lacks investigation, review, and reflection.

CL involves bringing attention to thoughts and sensations; responding with intentional, healthy, appropriate decisions; and continually reviewing to achieve a virtuous cycle of learning and evolving our habits and patterns.

## CL framework

The CL resources provided here describe concepts and methods for achieving a more conscious and healthy life. Although this collection will always be incomplete, the structure is holistic enough to potentially incorporate any insights and helpful ideas from anywhere. This is itself a living project that can continually adapt and improve.

## Consciousness

Consciousness means having first-person subjective experience — that it *feels like something* to be alive. As a term, "conscious" can also imply meta-awareness, as in recognizing, giving *attention* to our conscious experience. In CL, we further emphasize using this awareness *intentionally*, emphasizing volition and agency. To act consciously means to consider, to deliberate, and to make our decisions with thoughtful and embodied awareness.

The vast majority of our behaviors will always be unconscious. However, by taking care of our conscious processes, we set up contexts that can influence all the parts of our lives.

## CL Principles

The Principles are the main concepts and framing for CL. They include foundational concepts and core CL commitments.

As with all concepts, the CL ideas are abstractions and guidelines. All topics influence each other. The patterns and hierachies described here are not real or true in any fundamental sense. The structure's purpose is simply to make CL practical and accessible.

## CL Practice

In living moment-to-moment and day-to-day, practical ideas and tools help us to live up to the commitments, to improve our skills and understandings, and to make healthy decisions.

Life continually fluctuates and changes, and we can appreciate the process, engaging fully in the journey. While practicing CL, we still get distracted; we get out of balance; we may overreact or underreact; we may thrive, and we may suffer. Whatever happens, CL practices help us understand, accept, learn, and adapt.
