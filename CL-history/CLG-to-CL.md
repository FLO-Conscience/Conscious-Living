# Reframing the CLG 15 Commitments

All the ideas discussed here are explained directly in the [main CL descriptions](Conscious-Living-Principles/Conscious-Living-Principles.md). This is a historic discussion of how CL ideas were adapted by starting from the CLG framework.

## 15 Commitments of Conscious Leadership: background summary

The [Conscious Leadership Group](https://conscious.is) (CLG) is an executive coaching organization whose founders curated an inspiring synthesis of ideas from several sources (which of course were all themselves influenced by other sources). CLG's framework draws mainly from the [Hendricks Institute](https://hendricks.com). They also added in [The Work of Byron Katie](https://thework.com/), [The Sedona Method](https://www.sedona.com), [GTD](https://gettingthingsdone.com/), and several other teachings (including contemporary psychology, Stoicism, Buddhism, and more). They were also informed by their years of experience in coaching and their own life experiences including Christian ideas and much more.

CLG settled on a list of 15 Commitments that they felt covered the range core practices for living and leading in conscious, ethical, and effective ways. They also include broader ideas that don't directly fit into their 15 Commitments.

Since the publication of their book *The 15 Commitments of Conscious Leadership* in 2014, CLG has published a wide range related resources (blog articles, PDFs, videos, guided meditations, podcast interviews, and public examples of coaching sessions) at [conscious.is/resources](https://conscious.is/resources). They have continued to add new angles and nuances and updated materials over the years, though they have not updated the core 15 Commitments.

Some of the many features we appreciate about CLG's work:

- Curation of many wisdom practices and ethical mandates
- Particularly clear, concise structure
- Ideas framed in simple and memorable ways with precise and contemporary language, making it easier to access the ideas even during our most reactive moments 
- Embracing abundance-mentality and sharing (publishing resources freely without ads or paywalls)

### Creativity and FLO

Many aspects of CL point to the importance of FLO (Free/Libre/Open) ethics and methods. FLO is about win-for-all, seeing the world as allies, being motivated by play and love, embracing sharing and abundance, and so on.

CLG curated their framing from many existing resources, and our adaptations continue that pattern. Everything we do can be adapted as well. We are doing all we can to further facilitate creative collaboration by publishing with a recognized FLO license (CC BY-SA), sharing source files, and using collaboritive tools.

Had CLG more fully understood FLO principles, then we might have simply joined that effort. We know that their hearts are in the right place. In 2018, they reviewed their own integrity with their stated values and came to a [decision not to paywall or limit](https://conscious.is/blogs/walking-our-talk-a-whole-body-yes-business-decision-and-new-resources-for-you) access or general use of their materials. They did not go all the way to sharing source files and embracing collaboration.[^CLG-FLO-email]

For better or worse, we found ourselves called to lead this next step of adapting these ideas into a more collaborative, open process. As a FLO project, all the resources here can be continually improved, and everyone interested can participate — including anyone from CLG. CLG could also adapt our updates back into their materials if they wish. Anyone using the materials need only follow the CC BY-SA requirements.

[^CLG-FLO-email]: In March 2020, Aaron sent CLG this message (both in a survey response and later by email):

    > You mentioned "open source" in the webinar and in recently recorded meditation recordings. I would deeply appreciate if you would go all the way to embracing what that means. It has a formal definition which matters. Open Source is not merely Open Access. Right now, you are doing Open Access, allowing people to get materials and to share them. Open Source means enabling others to more readily contribute and adapt.
    > 
    > So, as much as I love your book, I see a lot of room for improvement. It has lots of reference to business CEOs and such. It's not quite oriented toward students or artists or journalists etc. I see room for improvement in the writing, room for editing. I would love to be able to get a copy of the *source* files under an actual Open Source license (in the case of this type of material CC BY-SA would be best, it requires that anyone making any derivative work must include credit to you *and* keep the license to pass the freedoms on to others, anyone who wanted to use the work in a restrictive fashion would need special copyright permission).
    > 
    > Open Source is an ultimate abundance-mentality, win-for-all way of producing publications. You understand so clearly the value of utilizing all the best resources and giving credit to your sources. You are on the right track in so many ways. I urge you to be open and curious and consider truly embracing this all the way, to go farther release your work under Open Source terms. It could bring an experience of potential, a spread of the ideas, creative output using your work that you might not yet even imagine. I would be happy to help (I have some expertise here). Please let me know any questions or if you want to discuss this with me any further.

    In May 2020, Aaron sent this follow-up:

    > I wanted to offer a bit of meta feedback:
    >
    > My enthusiasm for CLG and for promoting your website and work has been really enhanced by the ability to link to direct resources, to feel transparent in the connection online.
    > 
    > I was just noticing my aggravation and discomfort at a different website that has an obnoxious registration-wall in order to download hyped-up PDFs which turned out to be just summaries of stuff in their book, similar to the documents CLG has on the website.
    > 
    > Instead of wanting to buy their book or spread the word about the other org, I'm now discouraged. I don't want to send registration-wall links to friends and colleagues. I feel manipulated and less trusting of the relationship with that other org now.
    > 
    > I really appreciate how CLG lives up to the abundance mentality and transparency by sharing resources clearly with no weird catch or marketing hook. This makes me 100 times more happy to keep engaging with and promoting CLG.
    > 
    > I noticed also the use of "Open Source" as a term in recent new meditations, and I sent feedback in webinar surveys about how great it could be for CLG to actually embrace that fully. That would mean working to enable others to contribute creative improvements, derivatives, translations and so on under an appropriate license such as CC BY-SA. Doing that would be a next even-greater step in the direction you already have. I would be happy to discuss and help with this if you are open to considering this direction.
    > 
    > Mostly, just again thank you.
    > 
    > In harmony,
    > Aaron

    And then one more in June 2020:

    > On a related note (which I've mentioned in a couple feedback surveys but haven't heard any reply), I have a wish/request for CLG to really embrace Open Source (a term I've seen CLG throw out sometimes recently). There's steps in that direction, it doesn't have to be all-or-nothing. But the end goal of actually being Open Source by the definition would be that the source material is published under an appropriate license. The ideal workflow in the long-run looks like this: the word processing files that generate PDFs are themselves public and everything is under a CC BY-SA license, and it is all tracked in Git (or similar). Then, people who notice issues or improvements can make the changes directly and submit them for approval (merging). People could also more readily make translations to other languages etc, and the license would be retained for all derivatives, including an explicit credit to CLG. This is the ultimate abundance approach to sharing all this wisdom and maximizing the potential for it to get used in whatever creative ways we may not now even imagine.
    >
    > If at any point there's openness to even just having discussion to learn more about this sort of thing and the ethical, economic, and practical issues etc, I'd love to support that. Much of my work is in the Open Source movement (particularly beyond software and into other creative fields), and that's part of where I'm bringing in conscious leadership now.

    CLG replied finally, and along with saying thanks for Aaron's noting of various typos for them to fix, they acknowledged the FLO question this way:

    > [From CLG] Over here, we have been discussing ideas/thoughts that came up upon reading your message. We do not have immediate plans to change anything but are open to it and the discussions are certainly happening. I appreciate you and for asking for what you want.

## Adapting CLG and other frameworks

The CLG framework provided a great starting point. As a general rule, we continue to defer to CLG when we are unsure about any particular decisions for these updated CL ideas.

We also bring in ideas from other resources whenever they seem helpful. Most other resources are more focused and less comprehensive. If we learn about any other similar holistic frameworks, we will integrate the ideas where it seems helpful and highlight any references we find particularly significant.

Incidentally, when CLG finds new sources they want to highlight, they might blog about them, sometimes describing them in terms of the 15 commitments, sometimes not. CLG does not seem to consider revising the commitments themselves. We will not hesitate to change even the core structure of CL if new insights call for it.

The rest of this document explains the specific process of how the CL framework evolved from the CLG resources.

## Living is bigger than leadership

We felt some constriction around the "Leadership" label and its association with corporate executives and marketing. Of course, it's fine to talk of "leadership" for topics specific to leading. And good leaders should model all sorts of healthy practices, but that doesn't make the practices leadership-specific. Curiosity, feelings, appreciation, mindfulness… these aren't concerns only leaders would care about.

CLG defines "leadership" as anything *proactive* rather than *reactive*. That means that any time anyone is consciously deciding how to engage, that's "leadership" — even if the person works entirely alone in isolation and maybe even if their conscious, proactive decision is to follow someone else's leadership. With this definition, the only time we're not "leading" is when we're caught up in reactivity and acting through unconsidered impulses and habits. That almost makes "conscious" redundant.

Nothing in CLG's commitments is specific to leading organizations. It seems more honest, transparent, and practical to see all these practices as simply *living*. The question is whether we are *conscious* and thoughtful about how we treat ourselves, our families, our colleagues, or the rest of the world. There *are* topics out there which are more specific to organizational leadership such as recruitment, delegation, training others, facilitation and mediation, and more. The CLG Commitments are not specific to those topics.

While leadership is essential and important, our world often seems to overemphasize and celebrate leaders. We see importance in also highlighting and celebrating the bottom-up, grassroots dynamics of many movements. We can recognize the importance of all sorts of participants and relationships.

So, **we've changed CL to "Conscious Living" instead of "Conscious Leadership"**. Thankfully, both the acronym and the meaning still match that used by CLG. "Conscious Living" is simply a clearer and more easily understood label for exactly what CLG focuses on already.

Perhaps "living" will reduce the resistence that some people have to the "leadership" language that reminds them of superficial corporate marketing. On the other hand, the "conscious living" label may turn off conservative business folks or others who are skeptical of language they take as too "woo woo" mystical or similar (though perhaps such dismissal might be really driven by fears around facing uncomfortable parts of their lives). We certainly imagine it to be harder to convince corporate executives to budget funding for "Conscious Living" coaching compared to "Conscious Leadership" coaching. Anyway, our updated CL ideas do not prioritize marketing services to corporations. We aim for terms that will successfully reach the widest audience, and we accept the impossibility of perfection.

Note that **"conscious living" is not a reference strictly to this collection of ideas.** Conscious living is a general concept that different people can describe and practice in different ways. "Conscious Living" is itself a term and book title used by CLG's primary mentor, Gay Hendricks. Our CL collection is one effort to provide particularly effective, coherent, applicable, and memorable ideas for conscious living. While we have opinions about what counts as conscious living, nobody owns the concept, and nobody has ultimate authority about what counts. Besides, whatever attempts we make at defining ideas, our descriptions and understanding will be always evolving.

## Marking explicit CL Fundamentals

CLG includes in their materials several ideas that are not part of their 15 commitments. In particular, they use the core idea of a contrast between above and below "the line". We find it helpful to place such fundamentals clearly within the overall structure. We will mark CL *Principles* as including these other core ideas along with the commitments.

### "The Line" → Constriction and Expansion, Healthy-Appropriate

CLG makes the core starting point of their coaching being a simple line. At any moment you check whether you're above or below the line. The line represents the distinction between closed-reactive-triggered-righteous-threatened-etc and open-learning-curious-creative-loving-etc. It's useful that the model does not pick one simple word like "open" vs "closed" because that has a lot of connotations. Using just "the line" provides abstraction and encourages taking time to describe what it means. CLG resources explain below-the-line vs above-the-line for each commitment (e.g. blame vs responsibility).

Incidentally, we have not yet verified the original source, but "the line" idea might be part of Hendricks Institute ideas. It is out there in the world in other places. Some unrelated organization has a trademark on the terms "Above the Line" and "Below the Line" as mentioned in CLG's use policy. 

In working to quickly describe "the line" when teaching others, many different words came up. We tried "reactive" and "non-reactive" or "threatened" and "non-threatened". After some years of playing with different explanations, another reference from someone brought in "constriction" vs "expansion" and that really fits better. Those terms are more somatic, raw, and less burdened with cultural baggage. "Triggered" or "reactive" or "closed" all have more negative connotations than "constricted".

Constriction relates to other common terms like "contraction". We're going with "constriction" as the main term but will discuss various alternatives and their different implications in our full explanations. Diana Chapman of CLG herself discussed below-the-line states by writing, "Just the shift from contraction to openness can have a big impact…"

#### A continuum, not a border-line

We appreciate useful dichotomies, give-and-take, yin-yang, and so on. We can still see these contrasts in terms of continuums rather than categories.

Constriction and Expansion are energies we feel, and different parts of us can feel different amounts of each at the same time. Instead of "where are you? above or below the line?" we can say, "how are you? more constricted or expanded?" and the answer can be nuanced like "I'm feeling some constriction" or "I feel constriction in this area and expansion in this other respect". We can think of constriction as one pole and expansion as the other. We're never fully constricted or fully expanded. We feel varying degrees of each energy in various ways.

As we explored further, we felt that a two-dimensional model with *energy level* really brought out the best understanding. Expansion is energetic, motivation to play and learn and so on. It's not the same as relaxation and stillness.

#### Avoiding implications that constriction is bad or wrong

CLG constantly asserts that "below-the-line" is not bad or wrong. After initially acknowledging being below-the-line, their next step is to find *acceptance* and even *appreciation* for being below-the-line. They reliably point out that "below-the-line is bad" is just a story and not something true or unarguable. They explain that criticizing yourself for being below-the-line just pushes you further below the line. All this is great.

However, they constantly imply (presumably unintentionally) that below-the-line is bad, unhelpful, counterproductive etc. They describe a key goal of "ending drama". They call drama "entertaining but a huge waste of time and energy". All the celebration of the above-the-line qualities implies strongly that the below-the-line states are harmful and worse. The behaviors and thoughts they describe as below-the-line are all conventionally seen as unethical and/or problematic.

In an unusually harsh example, they talk about [firing people who don't practice the commitments](https://conscious.is/blogs/firing-people-who-dont-practice-the-15-commitments). They don't carefully describe the problem as failing to review and learn and so on. Instead, they use the above-the-line descriptions as the behavior employees should display.

A manager following most of CLG's own teachings would actually take a better approach. When they see employees being below-the-line, the manager could do work with their *own* constrictions around the situation. Eat their projections, work through stories and feelings, and so on. Maybe they still end up making a conscious, healthy, above-the-line, whole-body-yes decision to fire certain employees, and that's fine.

CLG falls so often into below-is-bad implications that they seem to confuse themselves. CLG uses blunt imperative language like "Take responsibility, don’t blame and criticize". Some of their hand-outs flat out have headers that say "do" and "don't" above lists of "above the line" and "below the line" behaviors. That does not describe accepting and appreciating the blame and criticism and using CL processes to manage them. Out of context, a reader could too easily have the mistaken impression that practicing CLG's commitments means (almost) never being below the line.

In our CL descriptions, we recognize that CL practice begins the instant *some* conscious inquiry at any level breaks the cycle of constrictive reactivity. Once CL practice is part of the picture, we can see that constriction *always* has value.

Like pain, constrictions have messages and insights. They are signals about some problem. Constriction protects us, and it happens because we *care*. If we find no constriction, it could be there's no problem. Or it could be that despite a real problem, we lack constriction because of ignorance, delusion, or apathy. Instead of feeling satisfied with no constriction, we might want to double-check whether we've understood dangers and risks we might be oblivious to.

Expansion is not inherently good either. Someone inspired to do something reckless while oblivious to the risks is someone in need of constriction to moderate their view. Whether constriction or expansion are appropriate depends on the circumstances.

So, we can describe the values of constriction, the values of expansion and what to do constructively with both. We can get out of unhealthy cycles of constriction by resolving the underlying causes. By always emphasizing constriction as a useful signal, we can avoid causing extra constriction about constriction.

#### Appropriate and healthy

Often, the things CLG says not to do really *are* good guidelines; they really do describe better and worse choices. So, we do need some way to describe what is good or bad, what is recommended or discouraged.

Some amount of prescription is reasonable. Buddhist translations seemed worth considering: helpful/unhelpful, wholesome/unwholesome, skillful/unskillful. The term that really seemed most clear and neutral was "appropriate" (versus "inappropriate"). After using that for a while, it felt almost too generic and vague, and we ended up feeling good about adding "healthy" (versus unhealthy) as an alternative emphasis.

"Appropriate" implies a focus on responding to specific situations. "Healthy" does a good job at expressing the values CLG often pushes when they advocate for above-the-line ways of being. Healthy appropriateness enables us to get away from total relativism and to describe CL with clear directives.

As CLG mixes good/bad and open/closed aspects in "the line", translating to constriction/expansion and healthy/unhealthy requires some thoughtful judgments about each case. Overall, we find this expansion greatly clarifying. The pitfalls of a single line are so clear from the apparent contradictions that CLG falls into trying to use it for both purposes.

### The CL process

CLG's core process: 1. Locate Yourself, 2. Accept Yourself, 3. Are You Willing to Shift?, 4. How Will You Shift?

Strangely, CLG doesn't put this or any related process in a clear, accessible place. This process is not formally named or structured anywhere. It's not part of the commitments or presented as a key starting place. We can make the practice process a formal foundation for CL and improve it.

- Locate Yourself means getting present with our levels of constriction and expansion 

Sometimes, CLG rephrases this as "where are you?" (or "where am I?"). All sorts of variations also work, such as "how am I?" or "what constrictions are here?"

We agree with this starting point. Instead of "locate", our favorite simple term of many we've encountered is **attune**.

- Accepting Yourself means not getting constricted *about* constriction

We appreciate the emphasis on acceptance, and we love especially the full 4 A's (see [4A Meditation](https://conscious.is/meditation/4a-meditation-acknowledge-allow-accept-and-appreciate)):
    - Acknowledge
    - Allow
    - Accept
    - Appreciate

Side-note: When recalling this framework in any particular moment, it is all-too-easy to think acceptance is about accepting our circumstances. However, as implied by "Accept Yourself" and as the 4A guided meditation does, the process is acceptance *of the constriction itself*! Only after that can we potentially apply the same acceptance process toward whatever we are constricted about.

From the 4 A's, Acknowledgement is the same as Locate Yourself. Allow and Accept are two stages of the same overall idea. Appreciation is related to shifting (finding appreciation or things to appreciate is one way to shift more fully into expansion). So, the 4 A's function as an alternative framing for the process.

We recognize how essential acceptance is, but we've found that people have much less trouble accepting constriction when we avoid describing it in ways that make it sound bad. When CLG says below-the-line is source of drama, and drama is the thing we want to end, that sets people up to find it challenging and contradictory to accept and appreciate being below-the-line. When the story is instead that constriction comes from important, valuable reactions that protect what we care about and bring us important insights, accepting and appreciating the constriction follows naturally. So, even though we need acceptance, our updated framing makes it less necessary to highlight as a distinct step.

- Willing to Shift

CLG loves "willingness" questions, and they're fine. They actually fit anywhere and everywhere, so they are not specific to any particular point in the process. Every step in anything can have an "are you willing to…" pre-step. "Are you Willing to Locate Yourself?", "Are You Willing To Accept Yourself?", "Are You Willing To Choose How To Shift?" and so on (though maybe not "are you willing to check for willingness?" which is like "may I ask you a question?"). **We can highlight willingness as an important tool in general without making it a specific process step**.

-  How Will You Shift

This form of shifting plays too much into the binary above-below model, and it reinforces the idea that being above is the goal. Sometimes, we are expanded and need to notice problems and get constricted enough to work on them. Or expansion is great, and it is signaling for us to *do something* out of our loving, creative motivations.  **Whatever we notice when we attune, what we need next is the *resolution*.** The question could "how will we resolve this?"

Summarizing CLG's process, we get Locate-Accept-Shift. And since acceptance is itself a *resolution* of constriction-about-constriction, we feel that the fuzzy process from attuning to resolving can include acceptance. Our reworded process so far is thus Attune-Resolve.

CLG says that engaging below-the-line tends to lead to drama. That seems useful as an inhibitor to compulsive/impulsive action. However, clearly we need to engage below-the-line sometimes, such as getting to safety when in real immediate danger. We can instead discuss *appropriate* resolutions and avoiding overreaction or underreaction.

Since we don't know what direction is appropriate in any situation, **we can adapt the idea of "shift moves" to be "resolution tools"**. We can explicitly collect tools and organize them. The 4-A's can be listed as one example. Exploring the commitments is another approach. CLG mentions various other general tools in different places, and we'll note them.

Our updated process works well with emergencies and with unwillingness to shift. In emergencies, appropriate resolution is taking safety measures. If for some reason we are unwilling to attune to our constriction or to work on resolution, that unwillingness is itself more constriction. To do the CL process, we need only to start it at some level — even if that level is attuning to and resolving our constriction about constriction about constriction.

How can we assess appropriateness? After meditating on this, discussing, exploring, and testing the ideas in practice, **we found a missing stage: Review**. In order to best learn all we can from each experience, we need to review it, to see how it went and whether our responses worked out as we intended or predicted. With whatever the review brings, we can apply the process again.

So, **our updated process is Attune-Resolve-Review.** And we can explicitly highlight these relevant key points: The focus is on attuning to and resolving our *own* energy. Willingness questions are one way to explore how we feel. Attuning and resolving inherently includes acceptance.

Overreacting or underreacting are likely to lead to more problems compared to appropriate resolutions. We can gauge appropriateness through reviewing and can aim for appropriateness by anticipating how we predict our review will go. So, the idea of *healthy and appropriate* fits into understanding Attune-Resolve-Review.

### Context vs Content

CLG talks about their focus on context rather than content. They describe themselves as experts in conscious presence and process, not subject-matter experts in whatever other fields. We love this framing, so we list it as a foundational principle for CL. This highlights how CL applies to any topic and situation in life, and it clarifies that the specifics of various subjects are *outside* the scope of CL principles.

Constriction and expansion is the primary aspect of context that CL focuses on. So, we can put those ideas within the overall discussion of context-vs-content.

#### Style

In CLG's context discussions, they often emphasize a framing they got from Michael Bernand Beckwith describing four ways of relating self to the world: to-me, by-me, through-me, and as-me. To-me is a victim mindset with an external locus of control. By-me gives agency to the self, an internal locus of control. Through-me puts the self in context of the big picture, emphasizing connections, cooperation, and less self-centering. As-me goes beyond the duality of self-vs-other, seeing how the concept of "me" can *include* the entire context. We find no essential self in any of our specific body parts or sensations or thoughts. We might shift around to call this experience "no-me" since the label ceases to define anything (when a word means everything it might as well mean nothing). Note that this lack of *essential* distinction applies to *all* labels, to all language. So, as-me is a perspective of transcending or simply not using the concept of the self.

CLG draws their "line" between to-me and by-me. Thus, that becomes the focus of almost all their resources, including how they describe each commitment. This treats the ideas around through-me and as-me as advanced stages that they presume most people won't be ready for. We will not be stuck with that limited shift since we have no more line and instead have a continuum of constriction to expansion. Full expansion doesn't center the concept of "me", it gets toward the as-me or no-me perspectives. Incidentally, Jim at CLG acknowledged how his journey took him well beyond by-me dynamics, see especially <https://conscious.is/blogs/three-waypoints-on-the-journey>.

We won't use the four -me framework directly. We can instead adapt the valuable parts and get away from the self-centeredness. The framings are best seen as descriptions of how we use language and concepts. Our use of language then has indirect effects on our mindsets beyond language. When we use to-me, by-me, through-me, as-me, for-me, and other forms of language, it will influence our thoughts and can induce various experiences and attention-shifts. In CL, we will state explicitly that this aspect of context is about playing with language **style**.

Along with describing each of the -me styles, CLG lists the features of ***posture*, *experience*, *beliefs*, *key questions*, and *benefits***. We see those as important items to independently highlight in discussing context.

Posture could also be called (with different emphasis for each) "attitude" or "mindset". The emphasis is on the flexibility and significance of language (including tone and such not expressed or captured in written language). We will expand this foundation to recognize the impacts of language styles like: passive or active; 1st, 2nd, or 3rd person; declarative, interrogative, or imperative; and others.

Incidentally, CLG advocates for the Enneagram personality-type model which defines nine personality categories and their interactions. Nowhere do they discuss how this fits with any of the rest of their framing. To the extent we care to discuss personality, we can fit that into style. Personality is a description of tendences in terms of style/mindset/posture.

We are not convinced about the Enneagram specifically, and personality psychology works better by emphasizing *traits* rather than *types*. The pitfalls of category labels outweigh any benefits. The issue is similar to the problems with strict above-or-below-the-line compared to the spectrum from consriction to expansion. We can best discuss personality with respected models like "Big 5" (OCEAN) or HEXACO lists of traits.

#### Stories and beliefs

**Beliefs** are related to worldview, paradigms, philosophy… these are also a major part of the context from which we engage in life. So, we will add discussion of that topic in terms of the importance it has to context.

We don't need to talk about experiences, questions, and benefits for these categories of by-me, to-me, through-me, as-me since we won't use those as foundational categories. We can describe the experiences, questions, and benefits of constriction and expansion in general, and we can describe them for constriction and expansion in terms of the various commitments as well. The various -me stories are just useful tools within the idea of exploring and adapting stories in general, alongside the idea of consider opposite stories.

The bigger issue we're grappling with here is the entire idea of "self". A broad term for what's going on with that might be "personal stories". We have these stories about who we are, how we are, how we relate to the world, and so on. Our stories affect how we engage with and think about things. So, we acknowledge the personal stories and ideas about identity and so on as a key part of our context.

We also invite a less-fixed attitude to personal stories. We can change our stories without changing the world first. We can hold more lightly our identity labels and stories.

Personal stories are just one type of story framing. Beliefs are themselves a sort of story. Overall, we can discuss **stories** in general as a dynamic.

Incidentally, CLG's focus on "drama" comes from *The Power of TED (The Empowerment Dynamic)* by David Emerald which itself draws on the [Karpman "drama triangle"](https://en.wikipedia.org/wiki/Karpman_drama_triangle). *TED* suggests an expansive framing of the triangle roles to contrast with Karpman's constrictive roles. The *TED* version is still drama though, in the sense that it is a story. The shift is from story stuck on conflict to a story about challenge, learning, and growth.

Karpman didn't want to use the name "conflict triangle" because he didn't want to suggest that people in a victim-mindset were necessarily *actual* victims. But we have found constriction (in both ourselves and in others) around using the term "drama" as CLG uses it. We think of "drama" as basically a synonym of "story" and cannot judge it healthy or unhealthy, good or bad. There are more or less helpful stories. Seeing them as stories at all helps us to play with them and not be stuck.

We can talk of "conflict" simply enough whenever describing actual conflicts of any sort. We can talk about what patterns cause conflicts (rather than "cause drama") and so on.

For the triangle models, we can discuss the **"conflict triangle"** and can ask questions like, "what role am I playing in the conflict story?". The explicit "story" label resolves the risk of accidentally validating the story. The *TED* name is mediocre (and obviously conflicts with "TED Talks") and probably came from needing a non-story label given using "drama" to be only the conflict-story version. "Empowerment dynamic" doesn't embrace what is useful about stories. We can call the more expansive triangle the **"inspiration triangle"** or "learning triangle".

We won't use "drama" much as a term except to describe any sort of acting and stories. We will use language describing constriction, conflict, tensions, problems, and similar as appropriate.

With "stories" noted as a specific aspect of context, we now have a place for where the triangle archetypes fit in describing CL ideas.

#### Other contextual factors

Style and stories/beliefs seem enough to capture these ideas CLG brings up. We see other topics to bring up as contexts, and we might expand the list as useful in the future.

**Habits** are patterns of behavior and thoughts that we have less direct awareness and control over. Habits are, by definition, patterns that happen without conscious decision. We can learn to observe habits and make decisions that end up changing our habits in constructive ways.

**Metaphors** are somewhat like style or beliefs but really they are a particular sort of conceptual connection that (usually subconsciously) influences how we see things. We think making our use of metaphor more explicit is helpful. It's also helpful to bring up specific useful metaphors that aid CL understanding.

We have noticed some other patterns worth calling out as contexts to recognize. **Trapping** is a type of pattern where things get stuck or do not have apparent ways out. For example, CLG has acknowledged the trap of cognitive-emotive-loops. We expand the overall concept using other sources and ideas as well.

## Restructuring the 15 Commitments

By defining their list of 15 Commitments[^commitments-definition], CLG summarizes and synthesizes ideas in memorable and actionable forms. Even though they need not be practiced in strict sequence, the list has an intentional order. Jim Dethmer explains in his article [*The Structure of the 15 Commitments*](https://conscious.is/blogs/the-structure-of-the-15-commitments). Their first focus is "reducing and ending drama". Next is directing the newly-freed energy that drama had been consuming. They end with bigger-picture existential ideas around seeing-truth and relating-to-the-world.

[^commitments-definition]: Language note: CLG chooses a definition of "commitment" that they sometimes stretch too far from common understanding. A charitable interpretation of their meaning is to see it as saying "deeds speak louder than words" — we can tell what someone's commitments are by watching how they live. CLG sometimes goes too far by skipping a key step: figuring out what is or isn't in our control. They almost push us to say that each of us are always committed to the status quo. They want to encourage the question, "how have my choices brought this status quo?" and to have people focus on what we *can* control. Consider a case like the world still having wars. It's radical enough to ask, "how are my choices part of the continuation of war?" as a way of emphasizing that there are likely more things most of us could do or change that relate to the topic. We don't see it as helpful to use the language, "I am committed to the continuation of war". In CLG's definition, whoever is the most dedicated anti-war peace activist on the planet still would say "I am committed to continual war" since that remains the status quo. We will stick to the more sensible common definition of "commitment" as meaning a *sincere promise* to uphold overall *values*. We can still somewhat evaluate the sincerity of one's intensions by observing the patterns of their concrete actions. Still, intentions matter and there are limits to what is under anyone's control.

As we worked on understanding their 15 Commitments, we noticed ways to adjust the structure to make it even clearer and more memorable.

### Adding the missing Commitment 0: Presence

Conscious awareness is at the heart of conscious living. CLG makes conscious presence such a top priority that they require prospective clients to have some form of mindfulness meditation practice. CLG does not put this explicitly in any structure, however. We feel it makes sense to have presence as the initial commitment.

With no other changes to the list, presence could be marked as "commitment 0". However, we have made many other structural changes. We might not keep an explicit numbering.

CLG doesn't seem to take a strong opinion about the various types of mindful practices. Different traditions and approaches offer different tools and insights. We will include relevant ideas from all the best sources we can find.

#### Feelings fit within presence and integrity

CLG's commitment 3 is feeling-your-feelings. We went years accepting this as a distinct commitment until we realized that this fits better as features of other commitments.

We find discussions of feelings important. Emotional intelligence and framings for experiencing and understanding feelings… We will still include all these ideas. We can mostly make them a subset of presence.

We can be present while resisting our feelings — if we are conscious of the resistance and the feelings being resisted. Still, the gist of presence is in allowance, is in getting toward accepting things. While that starts with accepting resistance itself, the trajectory is towards releasing constriction, relaxing, allowing feelings to flow without judgment. We otherwise can get curious about feelings and figure out how to respond — and those are other commitments. So, feeling-feelings isn't really a distinct commitment.

Feelings can also be considered in other commitments. Integrity includes feelings — we lack integrity when there are conflicts between feelings and actions or expressions. We can also be curious about feelings and take responsibility to resolve the issues that feelings relate to. Love and Compassion come largely as feelings, and our feelings inform us about issues relating to vitality.

### Responsibility and Curiosity

CLG's first two core commitments are Responsibility and Curiosity. We agree about these as essential and top-level.The choice to use CL processes at all is itself an act of taking responsibility. The process inherently requires curiosity.

### Putting the remaining Commitments in a hierarchy

- 4: Candor — great, but it just seems part of Integrity
- 5: Don't Gossip — *what is this?!* the only commitment with no positive angle

CLG seems to have failed to figure out an expanded (above-the-line) description for the topic of gossip. It sticks out sorely among all the other commitments. One reason they couldn't figure out "gossip" is because they already have candor as a separate commitment. The CLG gossip-commitment materials are less about ending gossip and more about going to people directly with candor.

Is there a point about gossiping that needs to be made distinct from Candor? Yes. The issue is *harmful* gossip where people talk about others with malicious intent, making out others to look bad, putting others down rather than helping.

The problem with gossip isn't whether people talk about others. And it's not about whether the talk is always something they'd be willing to share directly. Imagine someone dealing with harrassment who talks to others for the goal of making sense of their experience and to heal and to make decisions about their safety. Imagine that person sincerely wishing that the harrasser would resolve their own issues, wanting that person to thrive, holding no grudge or ill will. They might not feel safe talking to their harrasser directly. So, *harmful* gossip cannot be defined by whether or not you would say something about someone to their face.

**The missing commitment is Love and Compassion**. Gossiping is a *symptom*, it's not the principle. The commitment is about treating everyone (both present and absent) from a place of love and compassion. And that brings up many other valuable topics and practices that were missing in CLG's framework. A business executive could be faced with questioning whether they are engaging with love compassion when making decisions about product design, manufacturing, advertising, or employee wages. Adding this addresses a core concern we have about executive coaching: whether there are business leaders with mindfulness practices who treat their colleagues and families wonderfully while running businesses that do harm to the world.

- 6: Integrity — very important, but CLG's materials for it are mostly about making and keeping clear agreements. Their commitment might almost be renamed "agreements" or rather "good agreements".

We can keep the Integrity commitment, put Agreements as a specific section *within* Integrity and add in Candor (including the parts of Candor that were awkwardly put within Gossip) as another section.

- 7: Appreciation — super important, but now that we have Love and Compassion, we can make Appreciation a *part* of that.

- 8: Zone-of-Genius — super valuable, but it's also mostly part of Integrity.

When we're out of our zone, we aren't going to have as much whole-body-yes to our activities, we aren't being honest and candid with *ourselves*, we aren't in touch with our whole self.

Common use of "genius" implies smarter-than-others, so it's worth reducing that language at least in the title. We can still later describe the alternate meanings of "genius" as more like "genre" or "genus", more about being true to ourselves (and this clarification highlights how much this commitmnt is part of Integrity). We can call this commitment simply "zone" or "in the zone". The implication is that "zone of incompetence" and so on is not *in the zone* for us. The description could also use more emphasis on our capacity to get in the zone with various activities by changing *how* we engage with them to avoid labeling activities in a fixed way. So, Zone goes well enough within Integrity (though some aspects relate to Play and Rest). Now, Integrity is a much more fully-faceted and important commitment.

Side-note: there is a problem with CLG's idea that "zone" is something to deal with *after* drama (conflict) has ended. All the commitments can relate to experiencing conflict. We'll have conflict if we're out of our zone. The end-drama-then-use-freed-energy idea is okay as a general pattern, but it seems awkward and post-hoc to use it to describe the order of commitments. Resolving conflicts and using freed energy applies *within* any of the commitments.

- 9: Play and Rest — great, and doesn't belong within others, but needs expansion

All things are connected. We could say that treating ourselves with Love and Compassion will lead to being willing to honor play and rest and feeling our feelings, but those topics aren't *directly* about Compassion as much as they go with it. Play and Rest does not clearly fit within other commitments. However, like with gossip, it seems to be a part of a larger pattern.

Play and rest indicates something about rhythms and life-balance. It seems connected to other aspects of routines and cycles including nutrition, sleep, socializing and time alone, and so on. We can even work in conscious breathing and physical exercise as part of this commitment. Can we find an overall term that covers all these things? We did! **Vitality** is the overall term that best covers these topics, and it fits the linguistic pattern with integrity, curiosity, and responsibility. We can even add basically any topic related to physical health.

As we fleshed out the topics that go within Vitality, we realized that Play and Rest are not one topic but two among several others that go within the overall commitment.

- 10: Opposite of My Story — a specific practice, a tool for doing the CL process. It's a way explore our beliefs and get open-minded and then explore changing our views. Instead of a commitment, this is a resolution tool (a shift move technique) like 4-A's. 

- 11: Being the source of my needs (security, control, approval) — mixes claims about needs with responsibility or by-me style 
- 12: Experiencing having enough — about mindfulness in a sense, equanimity, but otherwise ties into appreciation and sharing

The needs emphasis of 11 is almost contradictory with 12. If we can at any moment truly experience having enough of everything, then we don't have *needs* to even be the source of in the first place. However, the sourcing-from-myself aspect of 11 is almost redundant with 12. If we can source our needs from ourselves internally, then we can just do that and have enough. Of course, we have a mixed experience (either over time or between different parts of us) with constriction around needs and expansion around mindful equanimity.

Sourcing from yourself connects to Responsibility. The emphasis is on not feeling helpless around external factors. We don't feel it fits as a separate item.

We feel "needs" can be more neutrally described as "drivers", meaning core motivations. For the list of core drivers, various wisdom traditions and psychological models have different ideas, many seem better than security, control, and approval. Anyway, we don't see the value in making the drivers list be an independent commitment. We just need to make sure the points are covered. "Control" is about agency, moving from to-me to by-me, and it relates to autonomy and freedom. We can tie that into Responsibility. Security and approval can fit into Vitality alongside other things we need to live healthy thriving lives.

The idea of having enough is really more about mindfulness in the moment. We don't necessarily have enough X in order to Y, that depends on the circumstances. Overall, we can recognize in the moment that we have enough of everything through mindful presence. So, we can move these ideas into Presence. The aspects that focus on abundance are really about *sharing*, and we can highlight that within Love and Compassion.

- 13: Allies, seeing the world as our ally — very valuable practice.
- 14: Win-for-all — also very important

Treating everyone as allies and seeking win-for-all are connected, and both are really part of Love and Compassion. The overarching principle here is that we aren't focusing on antagonism and zero-sum games but on cooperation, collaboration… the Love and Compassion commitment really takes all these specific items and gives them a clear and more-meaningful broader context.

What we *do* with the insights after seeing everything as our ally — that is more about CL process. It fits into the Attune stage, taking in the learning our allies offer us.

- 15: Being the Resolution — could be called "Resolve" and seems just style and responsibility

Committing to having resolve is redundant in terms of the meaning of "commitment" in the first place. Otherwise, it is mostly about emphasizing to-me vs by-me. Resolve is more of a style, an attitude, a posture toward things, and it's the same posture that responsibility emphasizes. It feels in the 15 commitments like a final conclusion to wrap it all up. We don't see the justification for making it a distinct commitment.

The Commitment 15 resources are actually mainly alternative guides through the Attune-Resolve-Review process, and we can incorporate the ideas there.

## Updated CL structure

Now, we have a much shorter and more memorable list that is still complete (given that we can specifically mark subitems):

- CL Principles
    - Attune-Resolve-Review
        - Healthy/Appropriate
    - Context vs Content
        - Constriction & Expansion
        - Style
            - including personality traits
        - Stories & Beliefs
            - including personal, societal, and otherwise
            - includes the conflict vs inspiration triangles
        - Habits
        - Metaphors
    - CL Commitments
        - Presence
            - includes fully allowing feelings
            - includes recognition of having enough
        - Responsibility
            - including resolve and sourcing-internally
        - Curiosity
        - Integrity
            - Candor
            - Sincerity
            - Agreements
            - Zone
        - Love & Compassion
            - Appreciation
            - Sharing
            - Win-For-All
            - World-as-ally
        - Vitality
            - Play (and humor)
            - Rest
            - Routines
            - Organizing
            - Physical health (breathing, fitness, nutrition, sleep, environment)
            - Social connection

So, we have 3 main principles, including just 6 core commitments. Those each fit within common working-memory limits. This new structure is clearer, simpler, more consistent, more memorable, and more comprehensive, while retaining all the important wisdom from CLG's framework.

## Separating out CL practice

While translating specific ideas and resources from CLG (and elsewhere) into the new CL framework, we found that various practical tools and guides often incorporate many different elements at once. So, we decided to explicitly mark CL-practice materials separately from the CL-principles. The *practice* collection can include any mix of tutorials, guides, and examples for the application of CL ideas. Where helpful, we will still note which principles are most relevant to various practice resources.

For example, willingness questions are a useful practice tool.

## What comes next

This structure may change further as CL continues to evolve. We will review relevant ideas from various sources, including CLG's sources (Hendricks Institute and so on). We will get feedback as CL principles are read by people and put to use. We plan to invite CLG folks themselves and anyone and everyone else to collaborate in this project.
