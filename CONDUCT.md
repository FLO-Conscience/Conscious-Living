# Code of Conscious Conduct 

*Work in progress version 0.2*

Participation in any space around the Conscious Living (CL) project(s) is governed by this Code of Conscious Conduct which is itself based on the [CL principles](https://codeberg.org/FLO-Conscience/Conscious-Living). In some sense, all of CL is relevant to and included within this Code.

We aspire to maintain a welcoming, safe, dynamic, and diverse community. To achieve that, we call on all participants to learn and practice CL.

We ask that participants practice the **attune-resolve-review** process explicitly. Before engaging in any way, attune to presence, noting thoughts and sensations. Consider choices consciously, and **aim for healthy and appropriate actions aligned with the CL commitments**. Review how interactions go, and continue the ongoing process of learning and improving. Support everyone else in their own conscious living process.

Recognize that engaging in a constricted state risks creating or exacerbating conflicts and problems. Please know that you are *welcome* even when feeling constricted. Just please process constriction in healthy and appropriate ways. Take a conscious breath, ask for assistance, and decide carefully how, when, and where to express concerns. Consider whether a response will be best public or private. If conflicts arise, work to *deescalate* and take appropriate responsibility, leading by example, modeling best CL practices.

Conscious conduct may include:

- editing and improving posts (and sometimes just deleting)
- asking others to edit and improve (or delete) their posts
- explicitly noting feelings and thoughts
- carefully labeling and separating observation and imagination
- expressing appreciation for others, independent of agreement
- accepting that we all have moods, habits, and other patterns that shift over time
- patience and forgiveness for everyone (including ourselves) as we learn and grow together

May the CL community be adaptable and resilient while maintaining integrity with our core values.

## Moderator practices 

Intervening to reduce risk of conflicts and other tensions is reasonable justification for moderator action. Moderators have the right to use their best judgments to remove (or otherwise manage, depending on tooling) posts that present a risk to maintaining healthy dynamics in the community. For example, especially in the case of newcomers, we can be extra sensitive to the risk of reduced participation if people have uncomfortable experiences. We also can consider the importance of maintaining the participation of current community members.

Moderators should follow CL practices themselves. Ideally bring up the issue with the poster as soon as appropriate, aiming to resolve issues, help the poster process their concerns, and check in with anyone else who might be reacting to a situation.

Moderators should review their actions and suggest any relevant updates to moderation practices and policies in order to improve things into the future.
 
May it never happen, but: Anyone who consistently refuses to work on CL practices may be asked to take a break from participation. As needed, moderators may take actions to limit or even block people if their participation is too unhealthy and they fail to make adequate changes voluntarily. We still aim to take such action with care and compassion, leaving open appropriate opportunities to reconcile or appeal the decision once any immediate risks or harms have been addressed. If such situations arise, may those affected find incentives and motivations to return later in good faith, doing all the necessary CL work to regain trust and become constructive participants.
