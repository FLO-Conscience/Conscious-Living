# Conscious Leadership Group Content Use Policy

*The [Conscious Leadership Group](https://conscious.is) publishes all their materials from their website (but not their book) under non-FLO yet relatively open terms. See [LICENSES.md](LICENSES.md) for further explanation and discussion of this policy and how it compares and contrasts with CC BY-SA.*

The following Policy, copied from <https://conscious.is/content-use-policy>, applies to all files in the Conscious Living repository that include attribution to "The Conscious Leadership Group".

## Content Use Policy

### Please use our content. Contribute as you see fit.

We want to support you to create a more conscious world with us. It is our intention to lead and participate in creating win for all solutions. This is why we choose to offer our online content (handouts, audio, and videos available at [www.conscious.is/resources](https://conscious.is/resources)) for you to use for educational, professional, and personal use. In return for the use of our content, we ask that you tune in and determine its value to you and to compensate us accordingly.

The win for you is that you can use our materials to support your goals and your business. The win for us is that we’re financially compensated for our time and energy for the content that we’ve created and continue to create.

You can contribute from scarcity and obligation, or from abundance and appreciation. Our invitation is for you to tune into what feels like a win for all, and to contribute from appreciation and an abundance mindset.

### Attribution

In every instance of content use, include an attribution to The Conscious Leadership Group.

If you don’t alter, or minimally alter, the form of the handouts (use your judgement), the attribution will read: “Created by The Conscious Leadership Group.”

If you significantly modify the handouts (use your judgement), the attribution will read: “Adapted from The Conscious Leadership Group.”

For all audio and video use, the attribution will read: “Created by The Conscious Leadership Group.”

If you combine our content with content from any other source, clearly attribute all sources content was derived from.

### Branding

You are welcome to layout the **handout content** in the style of your own brand. The attribution guidelines above apply.

We request that you don’t modify The Conscious Leadership Group branding on the **videos** if you use them in their entirety.

### Limitation: Do Not Sell Our Content.

The Conscious Leadership Group reserves the sole right to sell, or sell access to, any and all of our content, either individually or in combination with one another. 

### Warranties

The Conscious Leadership Group provides its content as is, without any express or implied warranties including, but not limited to warranties of merchantability, fitness for a particular purpose, or use or freedom from infringement or third party intellectual property rights.

In particular, ABOVE THE LINE® and BELOW THE LINE® are registered trademarks of Partners in Leadership, LLC.  The Conscious Leadership Group is not associated with or sponsored by Partners in Leadership, and does not have permission to use, nor the ability to license the use of the ABOVE THE LINE® and BELOW THE LINE® as trademarks. As a result, Conscious Leadership Group cautions all third parties against use of these trademarks in any way.
